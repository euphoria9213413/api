import { Workbook } from 'exceljs';
import { XlsxFileGenerateException } from 'src/exceptions/xlsx-file-generate.exception';
import * as tmp from 'tmp';
import * as xlsx from 'xlsx';

export class XlsxFileUtil {
  static async fileGenerate(data: any[]): Promise<unknown> {
    const rows = data.map((data) => Object.values(data));
    const book = new Workbook();
    const sheet = book.addWorksheet('sheet1');
    rows.unshift(Object.keys(data[0]));

    sheet.addRows(rows);

    return await new Promise((resolve, reject) => {
      tmp.file(
        {
          discardDescriptor: true,
          prefix: 'AEA-Vendas',
          postfix: '.xlsx',
          mode: parseInt('0600', 8),
        },
        (err, file) => {
          if (err) {
            throw new XlsxFileGenerateException();
          }

          book.xlsx
            .writeFile(file)
            .then(() => resolve(file))
            .catch(() => {
              throw new XlsxFileGenerateException();
            });
        },
      );
    });
  }

  static readFile(path: string) {
    const workbook = xlsx.readFile(path);
    const sheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[sheetName];
    const data = xlsx.utils.sheet_to_json(worksheet, { header: 1 });
    const dataWitoutWhiteRow = data.filter((row) =>
      Object.values(row).some((cell) => cell !== ''),
    );
    dataWitoutWhiteRow.shift();

    return dataWitoutWhiteRow;
  }
}
