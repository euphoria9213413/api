import { extname } from 'path';
import { FileTypeException } from 'src/exceptions/file-type.exception';

export const imageFileFilter = (
  req,
  file,
  callback,
  allowedExtensions: string[],
) => {
  const ext = extname(file.originalname);

  if (allowedExtensions.includes(ext)) {
    callback(null, true);
  } else {
    callback(new FileTypeException(allowedExtensions.join(', ')), false);
  }
};
