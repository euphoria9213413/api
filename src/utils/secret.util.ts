import { AES, enc } from 'crypto-js';
import env from 'src/config/env';

export class SecretUtil {
  static decrypt(value: string): string {
    return AES.decrypt(value, env.payment.payment_secret).toString(enc.Utf8);
  }
}
