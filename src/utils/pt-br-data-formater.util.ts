import { DateAndHourPeriod } from 'src/dtos/date.dto';

export class PtBrDataFormatterUtil {
  static roundFloat(value: number): string {
    return parseFloat(
      value
        .toLocaleString('en-US', { maximumFractionDigits: 2 })
        .replace(',', ''),
    ).toFixed(2);
  }

  static dateAndHourPeriodFormater(value: Date): DateAndHourPeriod {
    const result = value
      .toLocaleString('pt-BR', {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        timeZone: 'America/Sao_Paulo',
      })
      .replace(',', '')
      .split(' ');

    return {
      date: result[0],
      hour: result[1],
    };
  }

  static moneyFormat(price: number): string {
    return price.toLocaleString('pt-BR', {
      style: 'currency',
      currency: 'BRL',
    });
  }

  static dateAndHourFormater(value: Date): DateAndHourPeriod {
    const dateTime = value.toISOString().split('T');
    const date = dateTime[0].split('-');
    const time = dateTime[1].split(':');

    return {
      date: `${date[2]}/${date[1]}/${date[0]}`,
      hour: `${time[0]}:${time[1]}`,
    };
  }
}
