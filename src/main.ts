import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { config } from 'aws-sdk';
import env from './config/env';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.useGlobalPipes(new ValidationPipe());
  config.update({
    region: env.aws.region,
    accessKeyId: env.aws.bucket.access_key,
    secretAccessKey: env.aws.bucket.secret_access_key,
  });
  await app.listen(3000);
}
bootstrap();
