import { HttpException, HttpStatus } from '@nestjs/common';

export class XlsxFileGenerateException extends HttpException {
  constructor() {
    super('Falha ao gerar planilha', HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
