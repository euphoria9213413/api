import { HttpException, HttpStatus } from '@nestjs/common';

export class PermissionDeniedException extends HttpException {
  constructor() {
    super(
      'O perfil não possui as permissões necessárias',
      HttpStatus.FORBIDDEN,
    );
  }
}
