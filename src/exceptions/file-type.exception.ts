import { HttpException, HttpStatus } from '@nestjs/common';

export class FileTypeException extends HttpException {
  constructor(fileTypes: string) {
    super(
      `Tipo de arquivo inválido. Permitidos: ${fileTypes}`,
      HttpStatus.BAD_REQUEST,
    );
  }
}
