import { HttpException, HttpStatus } from '@nestjs/common';

export class AccountValidationException extends HttpException {
  constructor() {
    super(
      'Não é possível dar manutenção em conta de outro usuário',
      HttpStatus.FORBIDDEN,
    );
  }
}
