import { HttpException, HttpStatus } from '@nestjs/common';

export class AuthenticationException extends HttpException {
  constructor() {
    super('Credenciais incorretas', HttpStatus.UNAUTHORIZED);
  }
}
