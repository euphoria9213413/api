import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import env from './env';
import * as path from 'path';

export const databaseConfig: TypeOrmModuleOptions = {
  keepConnectionAlive: true,
  type: 'postgres',
  host: env.db.host,
  port: parseInt(env.db.port),
  username: env.db.user,
  password: env.db.password,
  database: env.db.database,
  entities: [__dirname + '/../**/*.entity.{js,ts}'],
  synchronize: false,
  migrations: [path.resolve(__dirname, './migrations/*{.ts,.js}')],
  migrationsRun: true,
};
