import { MigrationInterface, QueryRunner, Table, TableIndex } from 'typeorm';

export class CreatePaymentTable1693007705844 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        schema: 'public',
        name: 'payment',
        columns: [
          { name: 'id', type: 'integer', isPrimary: true, isGenerated: true },
          { name: 'type', type: 'varchar' },
          { name: 'transactionId', type: 'varchar', isUnique: true },
          { name: 'status', type: 'varchar' },
          {
            name: 'value',
            type: 'numeric',
            precision: 10,
            scale: 2,
            default: 0,
          },
          { name: 'createdAt', type: 'timestamp', default: 'NOW()' },
          { name: 'updatedAt', type: 'timestamp', default: 'NOW()' },
        ],
      }),
    );

    // INDEXS CONSTRAINTS
    const transactionIdIndex = new TableIndex({
      name: 'payment_transaction_id_index',
      columnNames: ['transactionId'],
    });
    await queryRunner.createIndex('public.payment', transactionIdIndex);

    const typeIndex = new TableIndex({
      name: 'payment_type_index',
      columnNames: ['type'],
    });
    await queryRunner.createIndex('public.payment', typeIndex);

    const statusIndex = new TableIndex({
      name: 'payment_status_index',
      columnNames: ['status'],
    });
    await queryRunner.createIndex('public.payment', statusIndex);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('public.payment');
  }
}
