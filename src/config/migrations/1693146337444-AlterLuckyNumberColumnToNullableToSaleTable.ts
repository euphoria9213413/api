import { MigrationInterface, QueryRunner } from 'typeorm';

export class AlterLuckyNumberColumnToNullableToSaleTable1693146337444
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE public.sale ALTER COLUMN "luckyNumber" DROP NOT NULL',
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      'ALTER TABLE public.sale ALTER COLUMN "luckyNumber" VARCHAR NOT NULL',
    );
  }
}
