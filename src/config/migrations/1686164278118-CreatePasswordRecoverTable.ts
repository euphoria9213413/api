import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex,
} from 'typeorm';

export class CreatePasswordRecoverTable1686164278118
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        schema: 'public',
        name: 'password_recover',
        columns: [
          { name: 'id', type: 'integer', isPrimary: true, isGenerated: true },
          { name: 'accessToken', type: 'varchar' },
          { name: 'expirationDate', type: 'timestamp' },
          { name: 'accountId', type: 'integer', isUnique: true },
          { name: 'createdAt', type: 'timestamp', default: 'NOW()' },
          { name: 'updatedAt', type: 'timestamp', default: 'NOW()' },
        ],
      }),
    );

    // FOREIGN KEYS CONSTRAINTS
    const accountIdFk = new TableForeignKey({
      columnNames: ['accountId'],
      referencedTableName: 'public.account',
      referencedColumnNames: ['id'],
    });
    await queryRunner.createForeignKey('public.password_recover', accountIdFk);

    // INDEXS CONSTRAINTS
    const accountIdFkUniqueIndex = new TableIndex({
      name: 'account_id_fk_index',
      columnNames: ['accountId'],
      isUnique: true,
    });
    await queryRunner.createIndex(
      'public.password_recover',
      accountIdFkUniqueIndex,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('public.password_recover');
  }
}
