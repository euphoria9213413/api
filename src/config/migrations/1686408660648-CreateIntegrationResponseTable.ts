import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex,
} from 'typeorm';

export class CreateIntegrationResponseTable1686408660648
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        schema: 'public',
        name: 'integration_response',
        columns: [
          { name: 'id', type: 'integer', isPrimary: true, isGenerated: true },
          { name: 'body', type: 'jsonb' },
          { name: 'integrationRequestId', type: 'integer' },
        ],
      }),
    );

    // FOREIGN KEYS CONSTRAINTS
    const integrationRequestFk = new TableForeignKey({
      columnNames: ['integrationRequestId'],
      referencedTableName: 'public.integration_request',
      referencedColumnNames: ['id'],
    });
    await queryRunner.createForeignKey(
      'public.integration_response',
      integrationRequestFk,
    );

    // INDEXS CONSTRAINTS
    const storeIdIndex = new TableIndex({
      name: 'integration_request_id_index',
      columnNames: ['integrationRequestId'],
    });
    await queryRunner.createIndex('public.integration_response', storeIdIndex);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('public.integration_response');
  }
}
