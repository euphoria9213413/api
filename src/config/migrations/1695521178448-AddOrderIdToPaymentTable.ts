import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddOrderIdToPaymentTable1695521178448
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'public.payment',
      new TableColumn({
        name: 'orderId',
        type: 'varchar',
        isNullable: true,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('public.payment', 'orderId');
  }
}
