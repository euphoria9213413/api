import { MigrationInterface, QueryRunner, Table, TableIndex } from 'typeorm';

export class CreateIntegrationRequestTable1686407088809
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        schema: 'public',
        name: 'integration_request',
        columns: [
          { name: 'id', type: 'integer', isPrimary: true, isGenerated: true },
          { name: 'body', type: 'jsonb' },
          { name: 'type', type: 'varchar' },
          { name: 'situation', type: 'boolean' },
          { name: 'createdAt', type: 'timestamp', default: 'NOW()' },
          { name: 'updatedAt', type: 'timestamp', default: 'NOW()' },
        ],
      }),
    );

    // INDEXS CONSTRAINTS
    const typeIndex = new TableIndex({
      name: 'type_index',
      columnNames: ['type'],
    });
    await queryRunner.createIndex('public.integration_request', typeIndex);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('public.integration_request');
  }
}
