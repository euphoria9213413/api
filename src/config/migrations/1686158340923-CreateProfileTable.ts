import { MigrationInterface, QueryRunner, Table } from 'typeorm';
import { ProfileEntity } from 'src/domains/account/entities/profile.entity';

export class CreateProfileTable1686158340923 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        schema: 'public',
        name: 'profile',
        columns: [
          { name: 'id', type: 'integer', isPrimary: true, isGenerated: true },
          { name: 'type', type: 'varchar', isUnique: true },
          { name: 'description', type: 'varchar' },
        ],
      }),
      true,
    );

    await queryRunner.query('COMMIT TRANSACTION');

    await queryRunner.connection.getRepository(ProfileEntity).save([
      {
        type: 'administrador',
        description: 'Administrador',
      },
      {
        type: 'cliente',
        description: 'Cliente',
      },
    ]);

    await queryRunner.query('COMMIT TRANSACTION');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('public.profile');
  }
}
