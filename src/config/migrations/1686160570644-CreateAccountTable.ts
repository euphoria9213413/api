import { AccountEntity } from 'src/domains/account/entities/account.entity';
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex,
} from 'typeorm';

export class CreateAccountTable1686160570644 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        schema: 'public',
        name: 'account',
        columns: [
          { name: 'id', type: 'integer', isPrimary: true, isGenerated: true },
          { name: 'name', type: 'varchar' },
          { name: 'email', type: 'varchar', isUnique: true },
          { name: 'cpf', type: 'varchar', isUnique: true },
          { name: 'birth', type: 'date', isNullable: true },
          { name: 'password', type: 'varchar' },
          { name: 'contact', type: 'varchar', isNullable: true },
          { name: 'situation', type: 'boolean', default: true },
          { name: 'profileId', type: 'integer', default: 2 },
          { name: 'createdAt', type: 'timestamp', default: 'NOW()' },
          { name: 'updatedAt', type: 'timestamp', default: 'NOW()' },
          { name: 'deletedAt', type: 'timestamp', isNullable: true },
        ],
      }),
    );

    // FOREIGN KEYS CONSTRAINTS
    const foreignKey = new TableForeignKey({
      columnNames: ['profileId'],
      referencedTableName: 'public.profile',
      referencedColumnNames: ['id'],
    });
    await queryRunner.createForeignKey('public.account', foreignKey);

    // INDEXS CONSTRAINTS
    const emailUniqueIndex = new TableIndex({
      name: 'account_email_index',
      columnNames: ['email'],
      isUnique: true,
    });
    await queryRunner.createIndex('public.account', emailUniqueIndex);

    const cpfUniqueIndex = new TableIndex({
      name: 'account_cpf_index',
      columnNames: ['cpf'],
      isUnique: true,
    });
    await queryRunner.createIndex('public.account', cpfUniqueIndex);

    const nameIndex = new TableIndex({
      name: 'account_name_index',
      columnNames: ['name'],
    });
    await queryRunner.createIndex('public.account', nameIndex);

    const profileIdFkIndex = new TableIndex({
      name: 'profileId_fk_index',
      columnNames: ['profileId'],
    });
    await queryRunner.createIndex('public.account', profileIdFkIndex);

    await queryRunner.query('COMMIT TRANSACTION');

    await queryRunner.connection.getRepository(AccountEntity).save([
      {
        name: 'Master User',
        email: 'admin@euphoria.com.br',
        cpf: '00000000000',
        password:
          '$2a$15$JEsRfaX8oINEaQ5zrHPb0.NgQGi4pkZy/zF7tyXWjCnOKLr9aJl3m',
        situation: true,
        profile: { id: 1 },
      },
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('public.account');
  }
}
