import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex,
} from 'typeorm';

export class CreateSaleTable1686258322026 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        schema: 'public',
        name: 'sale',
        columns: [
          { name: 'id', type: 'integer', isPrimary: true, isGenerated: true },
          { name: 'luckyNumber', type: 'varchar', isUnique: true },
          { name: 'buyerName', type: 'varchar' },
          { name: 'buyerContact', type: 'varchar', isNullable: true },
          { name: 'buyerEmail', type: 'varchar', isNullable: true },
          { name: 'productId', type: 'integer' },
          { name: 'graduateId', type: 'integer' },
          { name: 'accountId', type: 'integer' },
          { name: 'createdAt', type: 'timestamp', default: 'NOW()' },
          { name: 'updatedAt', type: 'timestamp', default: 'NOW()' },
          { name: 'deletedAt', type: 'timestamp', isNullable: true },
        ],
      }),
    );

    // FOREIGN KEYS CONSTRAINTS
    const storeFk = new TableForeignKey({
      columnNames: ['productId'],
      referencedTableName: 'public.product',
      referencedColumnNames: ['id'],
    });
    const graduateFk = new TableForeignKey({
      columnNames: ['graduateId'],
      referencedTableName: 'public.graduate',
      referencedColumnNames: ['id'],
    });
    const accountFk = new TableForeignKey({
      columnNames: ['accountId'],
      referencedTableName: 'public.account',
      referencedColumnNames: ['id'],
    });
    await queryRunner.createForeignKeys('public.sale', [
      graduateFk,
      storeFk,
      accountFk,
    ]);

    // INDEXS CONSTRAINTS
    const productIdIndex = new TableIndex({
      name: 'sale_product_id_index',
      columnNames: ['productId'],
    });
    await queryRunner.createIndex('public.sale', productIdIndex);

    const graduateIdIndex = new TableIndex({
      name: 'sale_graduate_id_index',
      columnNames: ['graduateId'],
    });
    await queryRunner.createIndex('public.sale', graduateIdIndex);

    const accountIdIndex = new TableIndex({
      name: 'sale_account_id_index',
      columnNames: ['accountId'],
    });
    await queryRunner.createIndex('public.sale', accountIdIndex);

    const buyerNameIdIndex = new TableIndex({
      name: 'sale_buyer_name_index',
      columnNames: ['buyerName'],
    });
    await queryRunner.createIndex('public.sale', buyerNameIdIndex);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('public.sale');
  }
}
