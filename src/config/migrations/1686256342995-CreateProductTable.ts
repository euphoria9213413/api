import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex,
} from 'typeorm';

export class CreateProductTable1686256342995 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        schema: 'public',
        name: 'product',
        columns: [
          { name: 'id', type: 'integer', isPrimary: true, isGenerated: true },
          { name: 'name', type: 'varchar' },
          { name: 'unitaryValue', type: 'numeric', precision: 10, scale: 2 },
          {
            name: 'graduateMargin',
            type: 'numeric',
            precision: 10,
            scale: 2,
            isNullable: true,
          },
          { name: 'goalUnit', type: 'integer', isNullable: true },
          { name: 'goalAmount', type: 'numeric', precision: 10, scale: 2 },
          { name: 'storeId', type: 'integer' },
          { name: 'createdAt', type: 'timestamp', default: 'NOW()' },
          { name: 'updatedAt', type: 'timestamp', default: 'NOW()' },
          { name: 'deletedAt', type: 'timestamp', isNullable: true },
        ],
      }),
    );

    // FOREIGN KEYS CONSTRAINTS
    const storeFk = new TableForeignKey({
      columnNames: ['storeId'],
      referencedTableName: 'public.store',
      referencedColumnNames: ['id'],
    });
    await queryRunner.createForeignKey('public.product', storeFk);

    // INDEXS CONSTRAINTS
    const nameStoreIdIndex = new TableIndex({
      name: 'product_name_store_id_index',
      columnNames: ['name', 'storeId'],
      isUnique: true,
    });
    await queryRunner.createIndex('public.product', nameStoreIdIndex);

    const nameIndex = new TableIndex({
      name: 'product_name_index',
      columnNames: ['name'],
    });
    await queryRunner.createIndex('public.product', nameIndex);

    const storeFkIndex = new TableIndex({
      name: 'product_store_id_fk_index',
      columnNames: ['storeId'],
    });
    await queryRunner.createIndex('public.product', storeFkIndex);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('public.product');
  }
}
