import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex,
} from 'typeorm';

export class CreateStoreTable1686249228768 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        schema: 'public',
        name: 'store',
        columns: [
          { name: 'id', type: 'integer', isPrimary: true, isGenerated: true },
          { name: 'name', type: 'varchar', isUnique: true },
          { name: 'slug', type: 'varchar', isUnique: true },
          { name: 'initialDate', type: 'timestamp' },
          { name: 'finalDate', type: 'timestamp' },
          { name: 'raffleDate', type: 'timestamp', isNullable: true },
          { name: 'situation', type: 'boolean' },
          { name: 'raffleUrl', type: 'varchar', isNullable: true },
          { name: 'feePolicy', type: 'varchar' },
          { name: 'nickname', type: 'varchar', isNullable: true },
          { name: 'videoUrl', type: 'varchar', isNullable: true },
          { name: 'regulationUrl', type: 'varchar', isNullable: true },
          { name: 'description', type: 'text', isNullable: true },
          { name: 'desktopMediaId', type: 'integer', isNullable: true },
          { name: 'mobileMediaId', type: 'integer', isNullable: true },
          { name: 'createdAt', type: 'timestamp', default: 'NOW()' },
          { name: 'updatedAt', type: 'timestamp', default: 'NOW()' },
          { name: 'deletedAt', type: 'timestamp', isNullable: true },
        ],
      }),
    );

    // FOREIGN KEYS CONSTRAINTS
    const desktopMediaFk = new TableForeignKey({
      columnNames: ['desktopMediaId'],
      referencedTableName: 'public.media',
      referencedColumnNames: ['id'],
    });
    const mobileMediaFk = new TableForeignKey({
      columnNames: ['mobileMediaId'],
      referencedTableName: 'public.media',
      referencedColumnNames: ['id'],
    });
    await queryRunner.createForeignKeys('public.store', [
      desktopMediaFk,
      mobileMediaFk,
    ]);

    // INDEXS CONSTRAINTS
    const desktopMediaFkIndex = new TableIndex({
      name: 'store_desktop_media_id_fk_index',
      columnNames: ['desktopMediaId'],
    });
    await queryRunner.createIndex('public.store', desktopMediaFkIndex);

    const mobileMediaFkIndex = new TableIndex({
      name: 'store_mobile_media_id_fk_index',
      columnNames: ['mobileMediaId'],
    });
    await queryRunner.createIndex('public.store', mobileMediaFkIndex);

    const nameIndex = new TableIndex({
      name: 'store_name_index',
      columnNames: ['name'],
      isUnique: true,
    });
    await queryRunner.createIndex('public.store', nameIndex);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('public.store');
  }
}
