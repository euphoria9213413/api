import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddExpirationToPaymentTable1724717447868
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('public.payment', [
      new TableColumn({
        name: 'copyPaste',
        type: 'varchar',
        isNullable: true,
      }),
      new TableColumn({
        name: 'expiration',
        type: 'timestamp',
        isNullable: true,
      }),
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumns('public.payment', [
      'copyPaste',
      'expiration',
    ]);
  }
}
