import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateMediaTable1686248970692 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        schema: 'public',
        name: 'media',
        columns: [
          { name: 'id', type: 'integer', isPrimary: true, isGenerated: true },
          { name: 'name', type: 'varchar' },
          { name: 'uniqueName', type: 'varchar' },
          { name: 'directory', type: 'varchar' },
          { name: 'mimeType', type: 'varchar' },
          { name: 'url', type: 'varchar', isNullable: true },
          { name: 'createdAt', type: 'timestamp', default: 'NOW()' },
          { name: 'updatedAt', type: 'timestamp', default: 'NOW()' },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('public.media');
  }
}
