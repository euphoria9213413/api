import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableIndex,
} from 'typeorm';

export class CreateGraduateTable1686257600175 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        schema: 'public',
        name: 'graduate',
        columns: [
          { name: 'id', type: 'integer', isPrimary: true, isGenerated: true },
          { name: 'code', type: 'varchar' },
          { name: 'name', type: 'varchar' },
          { name: 'cpf', type: 'varchar' },
          { name: 'class', type: 'varchar' },
          { name: 'situation', type: 'boolean', default: true },
          { name: 'storeId', type: 'integer' },
          { name: 'createdAt', type: 'timestamp', default: 'NOW()' },
          { name: 'updatedAt', type: 'timestamp', default: 'NOW()' },
          { name: 'deletedAt', type: 'timestamp', isNullable: true },
        ],
      }),
    );

    // FOREIGN KEYS CONSTRAINTS
    const storeFk = new TableForeignKey({
      columnNames: ['storeId'],
      referencedTableName: 'public.store',
      referencedColumnNames: ['id'],
    });
    await queryRunner.createForeignKey('public.graduate', storeFk);

    // INDEXS CONSTRAINTS
    const nameCpfStoreIdIndex = new TableIndex({
      name: 'graduate_code_cpf_store_id_index',
      columnNames: ['code', 'cpf', 'storeId'],
      isUnique: true,
    });
    await queryRunner.createIndex('public.graduate', nameCpfStoreIdIndex);

    const codeIndex = new TableIndex({
      name: 'graduate_code_index',
      columnNames: ['code'],
    });
    await queryRunner.createIndex('public.graduate', codeIndex);

    const cpfIndex = new TableIndex({
      name: 'graduate_cpf_index',
      columnNames: ['cpf'],
    });
    await queryRunner.createIndex('public.graduate', cpfIndex);

    const nameIndex = new TableIndex({
      name: 'graduate_name_index',
      columnNames: ['name'],
    });
    await queryRunner.createIndex('public.graduate', nameIndex);

    const classIndex = new TableIndex({
      name: 'graduate_class_index',
      columnNames: ['class'],
    });
    await queryRunner.createIndex('public.graduate', classIndex);

    const storeFkIndex = new TableIndex({
      name: 'graduate_store_id_fk_index',
      columnNames: ['storeId'],
    });
    await queryRunner.createIndex('public.graduate', storeFkIndex);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('public.graduate');
  }
}
