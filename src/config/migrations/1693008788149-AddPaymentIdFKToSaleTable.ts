import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey,
} from 'typeorm';

export class AddPaymentIdFKToSaleTable1693008788149
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'public.sale',
      new TableColumn({
        name: 'paymentId',
        type: 'integer',
        isNullable: true,
      }),
    );

    // FOREIGN KEYS CONSTRAINTS
    const paymentFk = new TableForeignKey({
      columnNames: ['paymentId'],
      referencedTableName: 'public.payment',
      referencedColumnNames: ['id'],
    });
    await queryRunner.createForeignKey('public.sale', paymentFk);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('public.sale', 'paymentId');
  }
}
