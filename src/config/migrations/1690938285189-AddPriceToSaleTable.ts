import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddPriceToSaleTable1690938285189 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'public.sale',
      new TableColumn({
        name: 'price',
        type: 'numeric',
        precision: 10,
        scale: 2,
        default: 0,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('public.sale', 'price');
  }
}
