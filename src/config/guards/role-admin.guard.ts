import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ProfileTypeEnum } from 'src/domains/account/enums/profile-type.enum';
import { PermissionDeniedException } from 'src/exceptions/permission-denied.exception';

@Injectable()
export class RoleAdminGuard implements CanActivate {
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    if (request['account'].role !== ProfileTypeEnum.administrador)
      throw new PermissionDeniedException();

    return true;
  }
}
