import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AccountValidationException } from 'src/exceptions/account-validation.exception';

@Injectable()
export class AccountValidationGuard implements CanActivate {
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    const paramId = Number(request.params.id);
    const tokenId = request['account'].id;

    if (paramId !== tokenId) throw new AccountValidationException();

    return true;
  }
}
