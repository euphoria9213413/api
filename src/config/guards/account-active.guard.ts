import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AccountDataDto } from 'src/domains/account/dtos/account.dto';
import { AccountInactivatedException } from 'src/domains/account/exceptions/account-inactivated.exception';
import { AccountRepository } from 'src/domains/account/repositories/account.repository';
import { AuthenticationException } from 'src/exceptions/authentication.exception';

@Injectable()
export class AccountActiveGuard implements CanActivate {
  constructor(private readonly accountRepository: AccountRepository) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const body = request.body;

    let account = null as AccountDataDto;
    try {
      account = await this.accountRepository.findByEmail(body.email);
    } catch {
      throw new AuthenticationException();
    }

    if (!account.situation) throw new AccountInactivatedException();

    return true;
  }
}
