import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';
import env from '../env';
import { AuthenticationException } from 'src/exceptions/authentication.exception';
import { AccountService } from 'src/domains/account/services/account.service';
import { AccountInactivatedException } from 'src/domains/account/exceptions/account-inactivated.exception';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly jwtService: JwtService,
    private readonly accountService: AccountService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = this.extractTokenFromHeader(request);

    if (!token) {
      throw new AuthenticationException();
    }

    let payload = null;
    try {
      payload = await this.jwtService.verifyAsync(token, {
        secret: env.securit.jwt_secret,
      });

      request['account'] = payload;
    } catch {
      throw new AuthenticationException();
    }

    const account = await this.accountService.show(payload.id);

    if (!account.situation) throw new AccountInactivatedException();

    return true;
  }

  private extractTokenFromHeader(request: Request): string | undefined {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
  }
}
