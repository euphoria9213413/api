if (process.env.NODE_ENV === 'dev') {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  require('dotenv').config({
    path: '.env',
  });
}

export default {
  db: {
    user: process.env.PG_USER,
    password: process.env.PG_PASSWORD,
    port: process.env.PG_PORT,
    database: process.env.PG_DATABASE,
    host: process.env.PG_HOST,
  },
  securit: {
    first_account_password: process.env.FIRST_ACCOUNT_PASSWORD,
    hash_salt: process.env.HASH_SALT,
    jwt_secret: process.env.JWT_SECRET,
  },
  aws: {
    region: process.env.AWS_REGION,
    bucket: {
      banner_name: process.env.AWS_BANNER_BUCKET_NAME,
      document_name: process.env.AWS_DOCUMENT_BUCKET_NAME,
      access_key: process.env.AWS_BUCKET_ACCESS_KEY,
      secret_access_key: process.env.AWS_BUCKET_SECRET_ACCESS_KEY,
    },
  },
  mail: {
    mail_service: process.env.MAIL_SERVICE,
    password_recover_token_ttl: process.env.PASSWORD_RECOVER_TOKEN_TTL,
  },
  links: {
    ecommerce_link: process.env.ECOMMERCE_LINK,
  },
  payment: {
    pix_key: process.env.PAYMENT_PIX_KEY,
    client_id: process.env.PAYMENT_CLIENT_ID,
    client_secret: process.env.PAYMENT_CLIENT_SECRET,
    correlation_id: process.env.PAYMENT_CORRELATION_ID,
    certificate: process.env.PAYMENT_CERTIFICATE,
    private_key: process.env.PAYMENT_PRIVATE_KEY,
    credit_card: {
      maxipago_api: process.env.MAXIPAGO_API,
      maxipago_merchant_id: process.env.MAXIPAGO_MERCHANT_ID,
      maxipago_merchant_key: process.env.MAXIPAGO_MERCHANT_KEY,
    },
    payment_secret: process.env.PAYMENT_SECRET,
  },
  cron: {
    consult_payment: process.env.CRON_CONSULT_PAYMENT,
    payment_status: process.env.CRON_PAYMENT_STATUS,
  },
  smtp: {
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    token: process.env.RESEND_TOKEN,
    user: process.env.GMAIL_USER,
  },
};
