import { Module } from '@nestjs/common';
import { PaymentService } from './services/payment.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PaymentRepository } from './repositories/payment.repository';
import { StorageModule } from '../storage/storage.module';
import { CreditCardPaymentService } from './services/credit-card-payment';

@Module({
  imports: [TypeOrmModule.forFeature([PaymentRepository]), StorageModule],
  controllers: [],
  providers: [PaymentService, CreditCardPaymentService, PaymentRepository],
  exports: [PaymentService, CreditCardPaymentService],
})
export class PaymentModule {}
