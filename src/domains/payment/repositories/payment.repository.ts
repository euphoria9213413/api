import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { PaymentEntity } from '../entities/payment.entity';
import { PaymentDataDto, PaymentStoreDto } from '../dtos';
import { PaymentStatusEnum } from '../enums';

@Injectable()
export class PaymentRepository extends Repository<PaymentEntity> {
  constructor(private readonly dataSource: DataSource) {
    super(PaymentEntity, dataSource.createEntityManager());
  }

  async store(data: PaymentStoreDto): Promise<PaymentDataDto> {
    return await this.save(data);
  }

  async updateById(id: number, body: any): Promise<void> {
    await this.update({ id }, { ...body });
  }

  async getPendingPayment(): Promise<PaymentDataDto[]> {
    return await this.find({
      where: { status: PaymentStatusEnum.ATIVA },
      relations: ['sales.product.store', 'sales.graduate', 'sales.account'],
    });
  }

  async updateAllStatusByExpiration(): Promise<void> {
    await this.createQueryBuilder()
      .update()
      .set({ status: PaymentStatusEnum.CANCELADO })
      .where('status = :status', { status: PaymentStatusEnum.ATIVA })
      .andWhere("expiration < now() - INTERVAL '3 hours'")
      .execute();
  }
}
