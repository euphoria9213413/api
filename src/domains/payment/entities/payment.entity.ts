import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { PaymentStatusEnum, PaymentTypeEnum } from '../enums';
import { SaleEntity } from 'src/domains/sale/entities/sale.entity';

@Entity({
  schema: 'public',
  name: 'payment',
  synchronize: false,
})
export class PaymentEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment', {
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'varchar',
    name: 'type',
  })
  type: PaymentTypeEnum;

  @Column({
    type: 'varchar',
    name: 'transactionId',
  })
  transactionId: string;

  @Column({
    type: 'varchar',
    name: 'status',
  })
  status: PaymentStatusEnum;

  @Column({
    type: 'numeric',
    name: 'value',
  })
  value: string;

  @Column({
    type: 'varchar',
    name: 'orderId',
  })
  orderId: string;

  @Column({
    type: 'varchar',
    name: 'copyPaste',
  })
  copyPaste: string;

  @Column({
    type: 'timestamp',
    name: 'expiration',
  })
  expiration: Date;

  @CreateDateColumn({
    type: 'timestamp',
    name: 'createdAt',
    default: () => 'NOW()',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    name: 'updatedAt',
    default: () => 'NOW()',
  })
  updatedAt: Date;

  @OneToMany(() => SaleEntity, (saleEntity) => saleEntity.payment, {
    persistence: false,
    cascade: false,
  })
  sales: SaleEntity[];
}
