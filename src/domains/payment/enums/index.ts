export enum PaymentTypeEnum {
  credit_card = 'credit_card',
  pix = 'pix',
}

export enum PaymentStatusEnum {
  ATIVA = 'ativa',
  CONCLUIDA = 'concluida',
  REMOVIDA_PELO_USUARIO_RECEBEDOR = 'removida_pelo_usuario_recebedor',
  REMOVIDA_PELO_APP = 'removida_pelo_app',
  DEVOLVIDO = 'devolvido',
  CANCELADO = 'cancelado',
  ERRO_403 = '403'
}
