import { Injectable } from '@nestjs/common';
import axios, { AxiosInstance } from 'axios';
import * as https from 'https';
import env from 'src/config/env';
import {
  ConsultPaymentStatusDto,
  GeneratedPixDto,
  PaymentDataDto,
  PaymentStoreDto,
} from '../dtos';
import { PaymentRepository } from '../repositories/payment.repository';
import { PaymentStatusEnum } from '../enums';
import { StorageService } from 'src/domains/storage/services/storage.service';
import { SaleStatusUpdateDto } from 'src/domains/sale/form-validations/sale.form-validation';
import { PtBrDataFormatterUtil } from 'src/utils/pt-br-data-formater.util';

@Injectable()
export class PaymentService {
  private cert;
  private key;

  constructor(
    private readonly paymentRepository: PaymentRepository,
    private readonly storageService: StorageService,
  ) {}

  async initSecurityFiles(): Promise<void> {
    this.key = await this.storageService.getSecuritFile(
      'ARQUIVO_CHAVE_PRIVADA2.key',
    );
    this.cert = await this.storageService.getSecuritFile('certificate2.crt');
  }

  private async getTokenApiInstance(): Promise<AxiosInstance> {
    if (!this.cert && !this.key) await this.initSecurityFiles();

    const httpsAgent = new https.Agent({
      cert: this.cert,
      key: this.key,
    });

    const apiHeaders = {
      Accept: 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Content-Type': 'application/x-www-form-urlencoded',
      'x-itau-flowID': 1,
      'x-itau-correlationID': 2,
      Cookie:
        'APIMANAGERSTATIC=cab42bac-81cb-4f69-90d9-0e7aa0231c3e; TS019a5128=01ca9250c74703c3762f28b968c88a0828dac95d8310f417c42a1b8addab03ad7ffb5caba469e1c8fd44a25fea2112553e240642c25c576357caa37c3aaddd66451625bde0; TS019fde4b=0151b3239231f278a97f196319bb8b3f04e35c26941f012d0e1bb0035339b714240d3236b8a9852c949031b84ae8f4dec8cd64e96bf15b1c206d520377ac4734fba4e1885b',
    };

    return axios.create({
      baseURL: 'https://sts.itau.com.br',
      timeout: 180000,
      httpsAgent: httpsAgent,
      headers: apiHeaders,
    });
  }

  private async getPaymentApiInstance(token?: string): Promise<AxiosInstance> {
    if (!this.cert && !this.key) await this.initSecurityFiles();

    const httpsAgent = new https.Agent({
      cert: this.cert,
      key: this.key,
    });

    const paymentHeaders = {
      Accept: 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'x-itau-apikey': env.payment.client_id,
      'x-itau-flowID': 1,
      'x-itau-correlationID': env.payment.correlation_id,
      Authorization: `Bearer ${token || (await this.getToken())}`,
    };

    return axios.create({
      baseURL: 'https://secure.api.itau/pix_recebimentos/v2',
      timeout: 180000,
      httpsAgent: httpsAgent,
      headers: paymentHeaders,
    });
  }

  async getToken(): Promise<string> {
    try {
      const formData = {
        grant_type: 'client_credentials',
        client_id: env.payment.client_id,
        client_secret: env.payment.client_secret,
      };

      const api = await this.getTokenApiInstance();
      const token = await api.post('/api/oauth/token', formData);

      return token.data.access_token;
    } catch (error) {
      console.log({ ...error });
      return error;
    }
  }

  async generatePixPayment(value: string): Promise<GeneratedPixDto> {
    try {
      const api = await this.getPaymentApiInstance();

      const params = {
        valor: { original: value },
        chave: env.payment.pix_key,
      };

      const payment = await api.post(`/cob`, { ...params });
      const criationDate = new Date(payment.data.calendario.criacao).getTime();

      return {
        txid: payment.data.txid,
        pixCopyPasteValue: payment.data.pixCopiaECola,
        expiration: new Date(
          criationDate + payment.data.calendario.expiracao * 1000,
        ),
      };
    } catch (error) {
      console.log({ ...error });

      return error;
    }
  }

  async consultPayment(
    txid: string,
    token: string,
  ): Promise<ConsultPaymentStatusDto> {
    try {
      const api = await this.getPaymentApiInstance(token);
      const payment = await api.get(`/cob/${txid}`);

      return {
        status: PaymentStatusEnum[payment.data?.status],
      };
    } catch (error) {
      console.log({ ...error });

      return error;
    }
  }

  async store(data: PaymentStoreDto): Promise<PaymentDataDto> {
    return await this.paymentRepository.store(data);
  }

  async getPendingPayments(): Promise<PaymentDataDto[]> {
    return await this.paymentRepository.getPendingPayment();
  }

  async update(id: number, status: string): Promise<void> {
    await this.paymentRepository.updateById(id, { status });
  }

  async statusUpdate(id: number, data: SaleStatusUpdateDto): Promise<void> {
    await this.paymentRepository.updateById(id, data);
  }

  async updateAllStatusWhenExpired(): Promise<void> {
    await this.paymentRepository.updateAllStatusByExpiration();
  }
}
