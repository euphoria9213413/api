import { Injectable } from '@nestjs/common';
import axios, { AxiosInstance, AxiosResponse } from 'axios';
import env from 'src/config/env';
import {
  CreditCartPaymentResponseDto,
  SendCreditCartPaymentDto,
} from '../dtos';
import { creditCartPaymentMapper } from '../mappers/request-data.mapper';
import { PtBrDataFormatterUtil } from 'src/utils/pt-br-data-formater.util';

@Injectable()
export class CreditCardPaymentService {
  private api: AxiosInstance;

  constructor() {
    const headers = {
      Accept: 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Content-Type': 'application/json',
    };

    this.api = axios.create({
      baseURL: env.payment.credit_card.maxipago_api,
      headers,
    });
  }

  async sendPayment(
    data: SendCreditCartPaymentDto,
  ): Promise<AxiosResponse<CreditCartPaymentResponseDto, any>> {
    const mappedData = creditCartPaymentMapper(data);

    return this.api.post<CreditCartPaymentResponseDto>(
      '/UniversalAPI/postXML',
      mappedData,
    );
  }

  getCalculatedAmount(amount: number, installment: number): string {
    switch (installment) {
      case 1:
        return PtBrDataFormatterUtil.roundFloat(amount + amount * (3.5 / 100));

      case 2:
        return PtBrDataFormatterUtil.roundFloat(amount + amount * (5.5 / 100));

      case 3:
        return PtBrDataFormatterUtil.roundFloat(amount + amount * (6.5 / 100));

      case 4:
        return PtBrDataFormatterUtil.roundFloat(amount + amount * (7 / 100));

      case 5:
        return PtBrDataFormatterUtil.roundFloat(amount + amount * (8 / 100));

      case 6:
        return PtBrDataFormatterUtil.roundFloat(amount + amount * (8.5 / 100));

      case 7:
        return PtBrDataFormatterUtil.roundFloat(amount + amount * (9.5 / 100));

      case 8:
        return PtBrDataFormatterUtil.roundFloat(amount + amount * (10.5 / 100));

      case 9:
        return PtBrDataFormatterUtil.roundFloat(amount + amount * (11 / 100));

      case 10:
        return PtBrDataFormatterUtil.roundFloat(amount + amount * (12 / 100));

      case 11:
        return PtBrDataFormatterUtil.roundFloat(amount + amount * (13 / 100));

      case 12:
        return PtBrDataFormatterUtil.roundFloat(amount + amount * (13.5 / 100));
    }
  }
}
