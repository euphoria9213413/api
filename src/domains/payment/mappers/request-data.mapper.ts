import env from 'src/config/env';
import { CreditCartPaymentRequestDto, SendCreditCartPaymentDto } from '../dtos';

export const creditCartPaymentMapper = (
  data: SendCreditCartPaymentDto,
): CreditCartPaymentRequestDto => ({
  'transaction-request': {
    version: '3.1.1.15',
    verification: {
      merchantId: env.payment.credit_card.maxipago_merchant_id,
      merchantKey: env.payment.credit_card.maxipago_merchant_key,
    },
    order: {
      sale: {
        processorID: '2',
        referenceNum: data.referenceNum,
        fraudCheck: 'N',
        ipAddress: data.ipAddress,
        transactionDetail: {
          payType: {
            creditCard: {
              number: data.cardNumber,
              expMonth: data.expirationDate.split('/')[0],
              expYear: data.expirationDate.split('/')[1],
              cvvNumber: String(data.secretNumber),
            },
          },
        },
        payment: {
          chargeTotal: data.amount,
          currencyCode: 'BRL',
          creditInstallment: {
            numberOfInstallments: data.installment,
            chargeInterest: 'N',
          },
        },
      },
    },
  },
});
