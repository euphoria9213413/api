import { SaleDataDto } from 'src/domains/sale/dtos/sale.dto';
import { PaymentStatusEnum, PaymentTypeEnum } from '../enums';

export type PaymentDataDto = {
  id: number;
  type: string;
  transactionId: string;
  status: PaymentStatusEnum;
  value: string;
  sales: SaleDataDto[];
  orderId?: string;
  copyPaste?: string;
  expiration?: Date;
  createdAt: Date;
  updatedAt: Date;
};

export type PaymentStoreDto = {
  type: PaymentTypeEnum;
  transactionId: string;
  status: PaymentStatusEnum;
  value: string;
  orderId?: string;
  copyPaste?: string;
  expiration?: Date;
};

export type GeneratedPixDto = {
  txid: string;
  pixCopyPasteValue: string;
  expiration: Date;
};

export type ConsultPaymentStatusDto = {
  status?: PaymentStatusEnum;
};

export type SendCreditCartPaymentDto = {
  ipAddress: string;
  cardNumber: string;
  expirationDate: string;
  secretNumber: string;
  referenceNum: string;
  installment: number;
  amount: string;
};

type Verification = {
  merchantId: string;
  merchantKey: string;
};

type CreditCard = {
  number: string;
  expMonth: string;
  expYear: string;
  cvvNumber: string;
};

type PayType = {
  creditCard: CreditCard;
};

type TransactionDetail = {
  payType: PayType;
};

type CreditInstallment = {
  numberOfInstallments: number;
  chargeInterest?: string;
};

type Payment = {
  chargeTotal: string;
  currencyCode: string;
  creditInstallment: CreditInstallment;
};

type Sale = {
  processorID: string;
  referenceNum: string;
  fraudCheck: string;
  ipAddress: string;
  transactionDetail: TransactionDetail;
  payment: Payment;
};

type Order = {
  sale: Sale;
};

type TransactionRequest = {
  version: string;
  verification: Verification;
  order: Order;
};

export type CreditCartPaymentRequestDto = {
  'transaction-request': TransactionRequest;
};

type TransactionResponse = {
  processorReferenceNumber: number;
  creditCardScheme: string;
  authCode: number;
  orderID: string;
  errorMessage: string;
  processorTransactionID: number;
  avsResponseCode: string;
  transactionID: number;
  transactionTimestamp: number;
  creditCardCountry: string;
  responseCode: number;
  referenceNum: number;
  creditCardBin: number;
  cvvResponseCode: string;
  processorMessage: string;
  processorName: string;
  responseMessage: string;
  processorCode: string;
  creditCardLast4: number;
};

export type CreditCartPaymentResponseDto = {
  'transaction-response': TransactionResponse;
};
