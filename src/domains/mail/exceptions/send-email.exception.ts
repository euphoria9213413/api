import { HttpException, HttpStatus } from '@nestjs/common';

export class SendEmailException extends HttpException {
  constructor() {
    super('Falha ao enviar e-mail', HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
