import { Injectable } from '@nestjs/common';
import * as nodemailer from 'nodemailer';
import env from 'src/config/env';
import * as locaweb from 'smtp-locaweb-nodejs';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import Mail from 'nodemailer/lib/mailer';

@Injectable()
export class MailService {
  private transporter: nodemailer.Transporter<SMTPTransport.SentMessageInfo>;

  constructor() {
    this.transporter = nodemailer.createTransport({
      host: env.smtp.host,
      secure: true,
      port: env.smtp.port,
      auth: {
        user: 'resend',
        pass: env.smtp.token,
      },
    } as any);
  }

  sendMailLocaweb(
    recipientEmail: string,
    emailTemplate: string,
    subject?: string,
    ccEmail?: string,
  ): void {
    const email = new locaweb.Email();

    email.addTo(recipientEmail);
    ccEmail && email.addCc(ccEmail);
    email.addSubject(subject ?? 'Ação Entre Amigos - Euphoria Formaturas');
    email.addFrom(env.smtp.user);
    email.addBody(emailTemplate);

    locaweb.sendMail(email);
  }

  async sendMailResend(
    recipientEmail: string,
    emailTemplate: string,
    subject?: string,
    ccEmail?: string,
  ): Promise<void> {
    const data = {
      from: env.smtp.user,
      to: recipientEmail,
      subject: subject ?? 'Ação Entre Amigos - Euphoria Formaturas',
      html: emailTemplate,
    } as Mail.Options;

    if (ccEmail) {
      data.cc = ccEmail;
    }

    await this.transporter.sendMail(data);
  }
}
