import env from 'src/config/env';

export const saleEmailTemplate = (
  name: string,
  luckNumber: string,
  purchaseDate: string,
  storeName: string,
  graduateSallerName: string,
  product: string,
  purchaseValue: string,
) =>
  `
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Seja Bem-vindo</title>
    </head>

    <body>
    <table border="0" cellpadding="50" cellspacing="0" width="100%">
        <tr>
            <td bgcolor="#dadada"></td>
            <td align="center" width="724" height="516" bgcolor="#dadada" style="padding: 0px 0 0px 0;">
    <img src="https://euphoriaformaturas.com.br/wp-content/uploads/2023/09/BANNER-4.png" alt="Bem-vindo" width="724" height="516" style="display: block;" /></td>
            <td bgcolor="#dadada"></td>
        </tr>
            <tr>
            <td bgcolor="dadada"></td>
            <td align="left" style="color:#F15A22; font-family:'Roboto', Black, sans-serif; font-style:bold; font-size: 34px; margin: 12px;"><b>Olá ${name}! Aqui é o time da Euphoria Formaturas</b>
              <p align="left" style="color:#545454; font-family:'Roboto', Roboto Bold, serif; font-style:Lucida Grande; font-size: 20px; margin: 5px;"> e estamos passando para avisar que você já está concorrendo a prêmios incríveis!🤩</p>
              <p></p>
              <p align="justify" style="color:#545454; font-family:'Roboto', Roboto Bold, sans-serif; font-style:Lucida Grande; font-size: 20px; margin: 10px;">Com essa compra, você ajuda os nossos formandos a realizarem o sonho de viver a jornada universitária com a Eupho! 🧡</p>
              <p></p>
              <p align="justify" style="color:#545454; font-family:'Roboto', Roboto Bold, sans-serif; font-style:Lucida Grande; font-size: 20px; margin: 10px;">Confira sua compra abaixo:</p>
              <p></p>
              <p align="justify" style="color:#545454; font-family:'Roboto', Roboto Bold, sans-serif; font-style:Lucida Grande; font-size: 20px; margin: 10px;">Seu número da sorte é <b style="color:#F15A22">${luckNumber}</b></p>
              <p align="justify" style="color:#545454; font-family:'Roboto', Roboto Bold, sans-serif; font-style:Lucida Grande; font-size: 20px; margin: 10px;">Data e hora da compra: ${purchaseDate}</p>
              <p align="justify" style="color:#545454; font-family:'Roboto', Roboto Bold, sans-serif; font-style:Lucida Grande; font-size: 20px; margin: 10px;">Nome da loja: ${storeName}</p>
              <p align="justify" style="color:#545454; font-family:'Roboto', Roboto Bold, sans-serif; font-style:Lucida Grande; font-size: 20px; margin: 10px;">Nome do formando vendedor: ${graduateSallerName}</p>
              <p align="justify" style="color:#545454; font-family:'Roboto', Roboto Bold, sans-serif; font-style:Lucida Grande; font-size: 20px; margin: 10px;">Produto: ${product}</p>
              <p align="justify" style="color:#545454; font-family:'Roboto', Roboto Bold, sans-serif; font-style:Lucida Grande; font-size: 20px; margin: 10px;">Valor da compra: ${purchaseValue}</p>
              <p align="left" style="color:#545454; font-family:Roboto , Roboto, sans-serif; font-style:Lucida; font-size: 18px; margin: 10;"><b>Boa sorte! Para dúvidas, acesse nosso site:</b> <a href=${env.links.ecommerce_link} target="_blank">${env.links.ecommerce_link}</a></p>
              <p></p>
              <p align="left" style="color:#545454; font-family:Roboto , Roboto, sans-serif; font-style:Lucida; font-size: 18px; margin: 10;"><b>Instagram:<a href="https://www.instagram.com/euphoria.formaturas/"> @euphoria.formaturas </a>│<a href="https://www.instagram.com/euphoriaplatinum/"> @euphoriaplatinum </a> <br>TikTok: <a href="https://www.tiktok.com/@euphoriaformaturas"> Euphoria Formaturas </a> <br>Site: <a href="https://euphoriaformaturas.com.br/"> www.euphoriaformaturas.com.br</a></b></p>
              </td>
            </td>
            <td bgcolor="dadada"></td>
        </tr>
        <tr>
          <td bgcolor="dadada"></td>
          <td bgcolor="dadada" align="center" style="color:#000000; font-family:'Roboto', Black, serif; font-style:bold; font-size: 12px; margin: 12px;">© Direitos reservados <b>Euphoria Formaturas</b></td>
          <td bgcolor="dadada"></td>
        </tr>
    </table>
    </body>
    </html>
  `;
