export const passwoerRecoverEmailTemplate = (
  name: string,
  accessToken: string,
  link_to_redirect: string,
) =>
  `
    <!DOCTYPE html>
    <html lang="pt-br">
      <head>
          <meta charset="utf-8"/>
          <title>Recuperação de senha</title>
      </head>
      <body>
        <h4>Olá ${name}</h4>
        <p>Recebemos uma solicitação para restaurar sua senha de acesso em nosso site.
        <p>Se você reconhece esta ação, clique no botão abaixo para prosseguir:</p>
        <br></br>
        <a href=${link_to_redirect}/alterar-senha/${accessToken} target="_blank"><h3><U><b>REDEFINIR SENHA</b></U></h3></a>
    </p>
      </body>
    </html>
  `;
