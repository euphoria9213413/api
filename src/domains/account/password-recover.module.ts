import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PasswordRecoverEntity } from './entities/password-recover.entity';
import { PasswordRecoverController } from './controllers/password-recover.controller';
import { PasswordRecoverService } from './services/password-recover.service';
import { PasswordRecoverRepository } from './repositories/password-recover.repository';
import { AccountModule } from './account.module';
import { MailModule } from '../mail/mail.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([PasswordRecoverEntity]),
    AccountModule,
    MailModule,
  ],
  controllers: [PasswordRecoverController],
  providers: [PasswordRecoverService, PasswordRecoverRepository],
})
export class PasswordRecoverModule {}
