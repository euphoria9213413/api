export enum ProfileTypeEnum {
  administrador = 'administrador',
  cliente = 'cliente',
}

export type ProfileTypeType = keyof typeof ProfileTypeEnum;
