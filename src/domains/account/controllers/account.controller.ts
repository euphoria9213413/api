import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AccountService } from '../services/account.service';
import {
  AuthenticationDto,
  AccountIndexPaginatedDto,
  AccountShowDto,
} from '../dtos/account.dto';
import {
  AccountEmailUpdateDto,
  AccountIndexQueryDto,
  AccountLoginDto,
  AccountPasswordUpdateDto,
  AccountSituationUpdateDto,
  AccountStoreDto,
  AccountEditDto,
} from '../form-validations/account.form-validation';
import { AuthGuard } from 'src/config/guards/auth.guard';
import { RoleAdminGuard } from 'src/config/guards/role-admin.guard';
import { AccountValidationGuard } from 'src/config/guards/account-validation.guard';
import { AccountActiveGuard } from 'src/config/guards/account-active.guard';

@Controller('account')
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @Get('index')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.OK)
  async index(
    @Query() query: AccountIndexQueryDto,
  ): Promise<AccountIndexPaginatedDto> {
    return await this.accountService.index(query);
  }

  @Get('by-token')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  async showByToken(@Req() request: any): Promise<AccountShowDto> {
    const id = request['account'].id;

    return await this.accountService.show(Number(id));
  }

  @Get(':id')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.OK)
  async show(@Param('id') id: string): Promise<AccountShowDto> {
    return await this.accountService.show(Number(id));
  }

  @Post('create')
  @HttpCode(HttpStatus.CREATED)
  async store(@Body() body: AccountStoreDto): Promise<void> {
    await this.accountService.store(body);
  }

  @Post('login')
  @UseGuards(AccountActiveGuard)
  @HttpCode(HttpStatus.OK)
  async doLogin(@Body() body: AccountLoginDto): Promise<AuthenticationDto> {
    return await this.accountService.login(body);
  }

  @Put('by-token')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  async editByToken(
    @Req() request: any,
    @Body() body: AccountEditDto,
  ): Promise<AuthenticationDto> {
    const id = request['account'].id;

    return await this.accountService.edit(Number(id), body);
  }

  @Put(':id')
  @UseGuards(AuthGuard, RoleAdminGuard, AccountValidationGuard)
  @HttpCode(HttpStatus.OK)
  async edit(
    @Param('id') id: string,
    @Body() body: AccountEditDto,
  ): Promise<AuthenticationDto> {
    return await this.accountService.edit(Number(id), body);
  }

  @Patch('/password-update/by-token')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  async passwordUpdateByToken(
    @Req() request: any,
    @Body() body: AccountPasswordUpdateDto,
  ): Promise<AuthenticationDto> {
    const id = request['account'].id;

    return await this.accountService.passwordUpdate(Number(id), body);
  }

  @Patch('email-update/by-token')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  async emailUpdateByToken(
    @Req() request: any,
    @Body() body: AccountEmailUpdateDto,
  ): Promise<void> {
    const id = request['account'].id;

    await this.accountService.emailUpdate(Number(id), body);
  }

  @Patch(':id/situation-update')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  async statusUpdate(
    @Param('id') id: string,
    @Body() body: AccountSituationUpdateDto,
  ): Promise<void> {
    await this.accountService.statusUpdate(Number(id), body);
  }

  @Patch(':id/password-update')
  @UseGuards(AuthGuard, RoleAdminGuard, AccountValidationGuard)
  @HttpCode(HttpStatus.OK)
  async passwordUpdate(
    @Param('id') id: string,
    @Body() body: AccountPasswordUpdateDto,
  ): Promise<AuthenticationDto> {
    return await this.accountService.passwordUpdate(Number(id), body);
  }

  @Patch(':id/email-update')
  @UseGuards(AuthGuard, RoleAdminGuard, AccountValidationGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  async emailUpdate(
    @Param('id') id: string,
    @Body() body: AccountEmailUpdateDto,
  ): Promise<void> {
    await this.accountService.emailUpdate(Number(id), body);
  }
}
