import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Param,
  Post,
} from '@nestjs/common';
import {
  PasswordRecoverDto,
  PasswordUpdateDto,
} from '../form-validations/password-recover.form-validation';
import { PasswordRecoverService } from '../services/password-recover.service';

@Controller('password-recover')
export class PasswordRecoverController {
  constructor(
    private readonly passwordRecoverService: PasswordRecoverService,
  ) {}

  @Post()
  @HttpCode(HttpStatus.OK)
  async store(@Body() body: PasswordRecoverDto): Promise<void> {
    await this.passwordRecoverService.store(body);
  }

  @Post(':accessToken')
  @HttpCode(HttpStatus.OK)
  async passwordUpdate(
    @Param('accessToken') accessToken: string,
    @Body() body: PasswordUpdateDto,
  ): Promise<void> {
    await this.passwordRecoverService.passwordUpdate(accessToken, body);
  }
}
