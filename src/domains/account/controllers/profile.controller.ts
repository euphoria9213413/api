import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { ProfileService } from '../services/profile.service';
import { AuthGuard } from 'src/config/guards/auth.guard';
import { RoleAdminGuard } from 'src/config/guards/role-admin.guard';
import { ProfileDataDto } from '../dtos/profile.dto';

@Controller('profile')
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @Get('index')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.OK)
  async show(): Promise<ProfileDataDto[]> {
    return await this.profileService.index();
  }
}
