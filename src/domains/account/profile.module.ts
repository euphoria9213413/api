import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProfileEntity } from './entities/profile.entity';
import { ProfileRepository } from './repositories/profile.repository';
import { ProfileController } from './controllers/profile.controller';
import { ProfileService } from './services/profile.service';
import { AccountModule } from './account.module';

@Module({
  imports: [TypeOrmModule.forFeature([ProfileEntity]), AccountModule],
  controllers: [ProfileController],
  providers: [ProfileService, ProfileRepository],
})
export class ProfileModule {}
