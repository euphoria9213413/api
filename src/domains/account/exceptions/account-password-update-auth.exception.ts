import { HttpException, HttpStatus } from '@nestjs/common';

export class AccountPasswordUpdateAuthException extends HttpException {
  constructor() {
    super('Credenciais incorretas', HttpStatus.FORBIDDEN);
  }
}
