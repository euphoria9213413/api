import { HttpException, HttpStatus } from '@nestjs/common';

export class SameAccountEmailException extends HttpException {
  constructor() {
    super(
      'O novo e-mail deve ser diferente do e-mail atual',
      HttpStatus.BAD_REQUEST,
    );
  }
}
