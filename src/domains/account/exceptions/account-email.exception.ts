import { HttpException, HttpStatus } from '@nestjs/common';

export class AccountEmailException extends HttpException {
  constructor() {
    super('O e-mail informado está incorreto', HttpStatus.BAD_REQUEST);
  }
}
