import { HttpException, HttpStatus } from '@nestjs/common';

export class PasswordRecoverAttemptException extends HttpException {
  constructor() {
    super(
      'O token para a redefinição de senha é inválido',
      HttpStatus.BAD_REQUEST,
    );
  }
}
