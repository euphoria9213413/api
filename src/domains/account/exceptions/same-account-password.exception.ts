import { HttpException, HttpStatus } from '@nestjs/common';

export class SameAccountPasswordException extends HttpException {
  constructor() {
    super(
      'A nova senha deve ser diferente da senha atual',
      HttpStatus.BAD_REQUEST,
    );
  }
}
