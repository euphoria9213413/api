import { HttpException, HttpStatus } from '@nestjs/common';

export class AccountEmailAlreadyExistsException extends HttpException {
  constructor() {
    super('Este E-mail já está cadastrado', HttpStatus.UNPROCESSABLE_ENTITY);
  }
}
