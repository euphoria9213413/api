import { HttpException, HttpStatus } from '@nestjs/common';

export class PasswordRecoverExpiredException extends HttpException {
  constructor() {
    super(
      'A redefinição de senha expirou, tente novamente',
      HttpStatus.BAD_REQUEST,
    );
  }
}
