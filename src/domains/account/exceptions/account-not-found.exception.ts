import { HttpException, HttpStatus } from '@nestjs/common';

export class AccountNotFoundException extends HttpException {
  constructor(id: number) {
    super(`Conta com o id ${id} não encontrada`, HttpStatus.NOT_FOUND);
  }
}
