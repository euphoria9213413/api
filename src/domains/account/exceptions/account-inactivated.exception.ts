import { HttpException, HttpStatus } from '@nestjs/common';

export class AccountInactivatedException extends HttpException {
  constructor() {
    super(
      `Conta inativada. Entre em contato com o suporte.`,
      HttpStatus.UNAUTHORIZED,
    );
  }
}
