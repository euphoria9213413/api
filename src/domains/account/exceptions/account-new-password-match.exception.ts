import { HttpException, HttpStatus } from '@nestjs/common';

export class AccountNewPasswordMatchException extends HttpException {
  constructor() {
    super(
      'A senha e confirmação de senha não conferem',
      HttpStatus.BAD_REQUEST,
    );
  }
}
