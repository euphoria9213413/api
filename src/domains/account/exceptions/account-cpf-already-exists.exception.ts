import { HttpException, HttpStatus } from '@nestjs/common';

export class AccountCpfAlreadyExistsException extends HttpException {
  constructor() {
    super('Este CPF já está cadastrado', HttpStatus.UNPROCESSABLE_ENTITY);
  }
}
