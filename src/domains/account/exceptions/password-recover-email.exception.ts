import { HttpException, HttpStatus } from '@nestjs/common';

export class PasswordRecoverEmailException extends HttpException {
  constructor() {
    super(
      'O e-mail informado não foi encontrado na nossa base de cadastro',
      HttpStatus.BAD_REQUEST,
    );
  }
}
