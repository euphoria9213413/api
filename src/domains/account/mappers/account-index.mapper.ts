import {
  AccountDataDto,
  AccountIndexDto,
  AccountIndexPaginatedDto,
} from '../dtos/account.dto';

export const accountIndexMapper = (
  data: AccountDataDto[],
  page: number,
  perPage: number,
  total: number,
): AccountIndexPaginatedDto => {
  const accounts = data.map(
    (account): AccountIndexDto => ({
      id: account.id,
      name: account.name,
      email: account.email,
      profile: account.profile,
      situation: account.situation,
    }),
  );

  return {
    data: accounts,
    page,
    perPage,
    lastPage: Math.ceil(total / perPage),
    total,
  };
};
