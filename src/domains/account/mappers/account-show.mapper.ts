import { AccountDataDto, AccountShowDto } from '../dtos/account.dto';

export const accountShowMapper = (data: AccountDataDto): AccountShowDto => ({
  id: data.id,
  name: data.name,
  email: data.email,
  cpf: data.cpf,
  birth: data.birth,
  contact: data.contact,
  situation: data.situation,
  profile: data.profile,
  createdAt: data.createdAt,
  updatedAt: data.updatedAt,
  deletedAt: data.deletedAt,
});
