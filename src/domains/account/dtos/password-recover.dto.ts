import { AccountDataDto } from './account.dto';

export type PasswordRecoverDataDto = {
  id: number;
  accessToken: string;
  expirationDate: Date;
  account: AccountDataDto;
  createdAt: Date;
  updatedAt: Date;
};

export type PasswordRecoverStoreDto = {
  accessToken: string;
  expirationDate: Date;
  account: number;
};
