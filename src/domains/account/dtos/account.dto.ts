import { ProfileDataDto } from './profile.dto';

export class AuthenticationDto {
  accessToken: string;
  id: number;
  name: string;
  profile: string;
}

export type AccountDataDto = {
  id: number;
  name: string;
  email: string;
  cpf: string;
  birth: Date;
  password: string;
  contact: string;
  situation: boolean;
  profile: ProfileDataDto;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
};

export type AccountIndexDto = {
  id: number;
  name: string;
  email: string;
  profile: ProfileDataDto;
  situation: boolean;
};

export type AccountIndexPaginatedDto = {
  data: AccountIndexDto[];
  page: number;
  perPage: number;
  lastPage: number;
  total: number;
};

export type AccountShowDto = Omit<AccountDataDto, 'password'>;
