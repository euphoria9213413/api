import { ProfileTypeType } from '../enums/profile-type.enum';

export type ProfileDataDto = {
  id: number;
  type?: ProfileTypeType;
  description?: string;
};
