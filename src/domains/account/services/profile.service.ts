import { Injectable } from '@nestjs/common';
import { ProfileRepository } from '../repositories/profile.repository';
import { ProfileDataDto } from '../dtos/profile.dto';

@Injectable()
export class ProfileService {
  constructor(private readonly profileRepository: ProfileRepository) {}

  async index(): Promise<ProfileDataDto[]> {
    return await this.profileRepository.getAll();
  }
}
