import { Injectable } from '@nestjs/common';
import { PasswordRecoverRepository } from '../repositories/password-recover.repository';
import {
  PasswordRecoverDto,
  PasswordUpdateDto,
} from '../form-validations/password-recover.form-validation';
import { AccountDataDto } from '../dtos/account.dto';
import { AccountRepository } from '../repositories/account.repository';
import { PasswordRecoverEmailException } from '../exceptions/password-recover-email.exception';
import env from 'src/config/env';
import { MailService } from 'src/domains/mail/services/mail.service';
import { passwoerRecoverEmailTemplate } from 'src/domains/mail/templates/password-recover-email.template';
import { RandomUtil } from 'src/utils/random.util';
import {
  PasswordRecoverDataDto,
  PasswordRecoverStoreDto,
} from '../dtos/password-recover.dto';
import { AccountService } from './account.service';
import { PasswordRecoverAttemptException } from '../exceptions/password-recover-attempt.exception';
import { PasswordRecoverExpiredException } from '../exceptions/password-recover-expired.exception';
import { AccountNewPasswordMatchException } from '../exceptions/account-new-password-match.exception';

@Injectable()
export class PasswordRecoverService {
  constructor(
    private readonly passwordRecoverRepository: PasswordRecoverRepository,
    private readonly accountService: AccountService,
    private readonly accountRepository: AccountRepository,
    private readonly mailService: MailService,
  ) {}

  async store(data: PasswordRecoverDto): Promise<void> {
    let account: AccountDataDto = null;
    try {
      account = await this.accountRepository.findByEmail(data.email);
    } catch {
      throw new PasswordRecoverEmailException();
    }

    const accessToken = RandomUtil.generateToken(60);
    const expirationDate = new Date();
    expirationDate.setMinutes(
      expirationDate.getMinutes() + Number(env.mail.password_recover_token_ttl),
    );

    const passwordRecoverData: PasswordRecoverStoreDto = {
      accessToken: accessToken,
      expirationDate: expirationDate,
      account: account.id,
    };
    await this.passwordRecoverRepository.storeOrUpdate(passwordRecoverData, [
      'account',
    ]);

    this.mailService.sendMailResend(
      data.email,
      passwoerRecoverEmailTemplate(
        account.name,
        accessToken,
        data.link_to_redirect,
      ),
    );
  }

  async passwordUpdate(
    accessToken: string,
    data: PasswordUpdateDto,
  ): Promise<void> {
    let passwordRecover: PasswordRecoverDataDto = null;
    try {
      passwordRecover = await this.passwordRecoverRepository.findByToken(
        accessToken,
      );
    } catch {
      throw new PasswordRecoverAttemptException();
    }

    const now = new Date();
    if (passwordRecover.expirationDate < now) {
      throw new PasswordRecoverExpiredException();
    }

    if (data.newPassword !== data.newPasswordConfirmation) {
      throw new AccountNewPasswordMatchException();
    }

    await this.accountService.validateSamePassword(
      data.newPassword,
      passwordRecover.account.password,
    );

    await this.accountService.passwordHashGenerateAndUpdate(
      passwordRecover.account.id,
      data.newPassword,
    );
  }
}
