import { Injectable } from '@nestjs/common';
import { AccountRepository } from '../repositories/account.repository';
import * as bcrypt from 'bcryptjs';
import env from 'src/config/env';
import {
  AuthenticationDto,
  AccountDataDto,
  AccountIndexPaginatedDto,
  AccountShowDto,
} from '../dtos/account.dto';
import { JwtService } from '@nestjs/jwt';
import { AccountEmailAlreadyExistsException } from '../exceptions/account-email-already-exists.exception';
import { AccountCpfAlreadyExistsException } from '../exceptions/account-cpf-already-exists.exception';
import {
  AccountEmailUpdateDto,
  AccountIndexQueryDto,
  AccountLoginDto,
  AccountPasswordUpdateDto,
  AccountSituationUpdateDto,
  AccountStoreDto,
  AccountEditDto,
} from '../form-validations/account.form-validation';
import { AccountNotFoundException } from '../exceptions/account-not-found.exception';
import { accountShowMapper } from '../mappers/account-show.mapper';
import { AccountNewPasswordMatchException } from '../exceptions/account-new-password-match.exception';
import { SameAccountPasswordException } from '../exceptions/same-account-password.exception';
import { AuthenticationException } from '../../../exceptions/authentication.exception';
import { SameAccountEmailException } from '../exceptions/same-account-email.exception';
import { AccountEmailException } from '../exceptions/account-email.exception';
import { RandomUtil } from 'src/utils/random.util';
import { AccountPasswordUpdateAuthException } from '../exceptions/account-password-update-auth.exception';

@Injectable()
export class AccountService {
  constructor(
    private readonly accountRepository: AccountRepository,
    private readonly jwtService: JwtService,
  ) {}

  async index(query: AccountIndexQueryDto): Promise<AccountIndexPaginatedDto> {
    return await this.accountRepository.getAllByQuery(query);
  }

  async show(id: number): Promise<AccountShowDto> {
    try {
      return accountShowMapper(await this.accountRepository.findById(id));
    } catch {
      throw new AccountNotFoundException(id);
    }
  }

  async store(data: AccountStoreDto): Promise<void> {
    await this.newAccountValidate(data.email, data.cpf);

    if (!data.password) {
      data.password = RandomUtil.generateToken(10);
    }

    const hash = await bcrypt.hash(
      data.password,
      Number(env.securit.hash_salt),
    );

    data.profile = { id: data.profileId };
    data.password = hash;

    await this.accountRepository.store(data);
  }

  private async newAccountValidate(email: string, cpf: string): Promise<void> {
    if (await this.accountRepository.existsByEmail(email)) {
      throw new AccountEmailAlreadyExistsException();
    }

    if (await this.accountRepository.existsByCpf(cpf)) {
      throw new AccountCpfAlreadyExistsException();
    }
  }

  async edit(id: number, data: AccountEditDto): Promise<AuthenticationDto> {
    try {
      await this.accountRepository.findById(id);
    } catch {
      throw new AccountNotFoundException(id);
    }

    data.profile = { id: data.profileId };

    const updatedAccount = await this.accountRepository.editById(id, data);

    return await this.generateToken(updatedAccount);
  }

  async login(data: AccountLoginDto): Promise<AuthenticationDto> {
    let account = null as AccountDataDto;
    try {
      account = await this.accountRepository.findByEmail(data.email);
    } catch {
      throw new AuthenticationException();
    }

    const isValidPassword = await bcrypt.compare(
      data.password,
      account.password,
    );

    if (!isValidPassword) throw new AuthenticationException();

    return await this.generateToken(account, data.rememberMe);
  }

  async statusUpdate(
    id: number,
    body: AccountSituationUpdateDto,
  ): Promise<void> {
    try {
      await this.accountRepository.findById(id);
    } catch {
      throw new AccountNotFoundException(id);
    }

    await this.accountRepository.updateById(id, body);
  }

  async passwordUpdate(
    id: number,
    body: AccountPasswordUpdateDto,
  ): Promise<AuthenticationDto> {
    let account: AccountDataDto = null;
    try {
      account = await this.accountRepository.findById(id);
    } catch {
      throw new AccountNotFoundException(id);
    }

    if (body.newPassword !== body.newPasswordConfirmation) {
      throw new AccountNewPasswordMatchException();
    }

    await this.validatePassword(body.currentPassword, account.password);
    await this.validateSamePassword(body.newPassword, account.password);

    await this.passwordHashGenerateAndUpdate(id, body.newPassword);
    const updatedAccount = await this.accountRepository.findById(id);

    return await this.generateToken(updatedAccount);
  }

  async passwordHashGenerateAndUpdate(
    id: number,
    newPassword: string,
  ): Promise<void> {
    const hash = await bcrypt.hash(newPassword, Number(env.securit.hash_salt));

    await this.accountRepository.updateById(id, { password: hash });
  }

  async emailUpdate(id: number, body: AccountEmailUpdateDto): Promise<void> {
    let account: AccountDataDto = null;
    try {
      account = await this.accountRepository.findById(id);
    } catch {
      throw new AccountNotFoundException(id);
    }

    this.emailValidate(account.email, body);

    await this.validatePassword(body.password, account.password);

    await this.accountRepository.updateById(id, { email: body.newEmail });
  }

  private emailValidate(
    accountEmail: string,
    body: AccountEmailUpdateDto,
  ): void {
    if (accountEmail !== body.currentEmail) {
      throw new AccountEmailException();
    }

    if (body.currentEmail === body.newEmail) {
      throw new SameAccountEmailException();
    }
  }

  private async validatePassword(
    password: string,
    accountPassword: string,
  ): Promise<void> {
    const isValidPassword = await bcrypt.compare(password, accountPassword);

    if (!isValidPassword) throw new AccountPasswordUpdateAuthException();
  }

  async validateSamePassword(
    newPassword: string,
    accountPassword: string,
  ): Promise<void> {
    const isSameCurrentPassword = await bcrypt.compare(
      newPassword,
      accountPassword,
    );

    if (isSameCurrentPassword) throw new SameAccountPasswordException();
  }

  private async generateToken(
    account: AccountDataDto,
    noExpiration = false,
  ): Promise<AuthenticationDto> {
    const payload = {
      id: account.id,
      userName: account.name,
      role: account.profile.type,
    };

    const tokenOptions = noExpiration ? { expiresIn: '1y' } : {};
    const token = await this.jwtService.signAsync(payload, tokenOptions);

    return {
      accessToken: token,
      id: account.id,
      name: account.name,
      profile: account.profile.type,
    };
  }
}
