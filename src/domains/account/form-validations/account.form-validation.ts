import {
  IsBoolean,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';
import { ProfileDataDto } from '../dtos/profile.dto';

export class AccountIndexQueryDto {
  @IsOptional()
  nameOrEmail?: string;

  @IsOptional()
  profileType?: string;

  @IsOptional()
  situation?: string;

  @IsNotEmpty({ message: 'Página deve ser informada' })
  page: string;

  @IsNotEmpty({ message: 'Quantidade por página deve ser informada' })
  perPage: string;
}

export class AccountStoreDto {
  @IsNotEmpty({ message: 'Nome deve ser informado' })
  @IsString({ message: 'Nome deve ser do tipo texto' })
  name: string;

  @IsEmail({}, { message: 'E-mail inválido' })
  email: string;

  @IsNotEmpty({ message: 'CPF deve ser informado' })
  @IsString({ message: 'CPF deve ser do tipo texto' })
  cpf: string;

  @IsOptional()
  birth?: Date;

  @IsOptional()
  @IsString({ message: 'Senha deve ser do tipo texto' })
  @MinLength(6, { message: 'Senha deve ter no mínimo 6 caracteres' })
  password: string;

  @IsOptional()
  @IsString({ message: 'Telefone deve ser do tipo texto' })
  contact?: string;

  @IsOptional()
  @IsBoolean({ message: 'Situação deve ser do tipo booleano' })
  situation: boolean;

  @IsOptional()
  profileId: number;

  profile: ProfileDataDto;
}

export class AccountEditDto {
  @IsNotEmpty({ message: 'Nome deve ser informado' })
  @IsString({ message: 'Nome deve ser do tipo texto' })
  name: string;

  @IsOptional()
  birth?: Date;

  @IsOptional()
  @IsString({ message: 'Telefone deve ser do tipo texto' })
  contact?: string;

  @IsBoolean({ message: 'Situação deve ser do tipo booleano' })
  situation: boolean;

  @IsOptional()
  profileId: number;

  profile: ProfileDataDto;
}

export class AccountLoginDto {
  @IsEmail({}, { message: 'E-mail inválido' })
  email: string;

  @IsNotEmpty({ message: 'Senha deve ser informada' })
  @IsString({ message: 'Senha deve ser do tipo texto' })
  password: string;

  @IsOptional()
  @IsBoolean()
  rememberMe: boolean;
}

export class AccountSituationUpdateDto {
  @IsNotEmpty({ message: 'Situação deve ser informada' })
  @IsBoolean({ message: 'Situação deve ser do tipo booleano' })
  situation: boolean;
}

export class AccountPasswordUpdateDto {
  @IsNotEmpty({ message: 'Senha atual deve ser informada' })
  @IsString({ message: 'Senha deve ser do tipo texto' })
  currentPassword: string;

  @IsNotEmpty({ message: 'Nova senha deve ser informada' })
  @IsString({ message: 'Nova senha deve ser do tipo texto' })
  @MinLength(6, { message: 'Nova senha deve ter no mínimo 6 caracteres' })
  newPassword: string;

  @IsNotEmpty({ message: 'Confirmação de nova senha deve ser informada' })
  @IsString({ message: 'Confirmação de nova senha deve ser do tipo texto' })
  @MinLength(6, {
    message: 'Confirmação de nova senha deve ter no mínimo 6 caracteres',
  })
  newPasswordConfirmation: string;
}

export class AccountEmailUpdateDto {
  @IsNotEmpty({ message: 'E-mail atual deve ser informado' })
  @IsEmail({}, { message: 'E-mail inválido' })
  currentEmail: string;

  @IsNotEmpty({ message: 'Novo e-mail deve ser informado' })
  @IsEmail({}, { message: 'E-mail inválido' })
  newEmail: string;

  @IsNotEmpty({ message: 'Senha deve ser informada' })
  @IsString({ message: 'Senha deve ser do tipo texto' })
  password: string;
}
