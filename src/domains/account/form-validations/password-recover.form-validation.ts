import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class PasswordRecoverDto {
  @IsNotEmpty({ message: 'E-mail deve ser informado' })
  @IsEmail({}, { message: 'E-mail inválido' })
  email: string;

  link_to_redirect: string;
}

export class PasswordUpdateDto {
  @IsNotEmpty({ message: 'Nova senha deve ser informada' })
  @IsString({ message: 'Nova senha deve ser do tipo texto' })
  @MinLength(6, { message: 'Nova senha deve ter no mínimo 6 caracteres' })
  newPassword: string;

  @IsNotEmpty({ message: 'Confirmação de nova senha deve ser informada' })
  @IsString({ message: 'Confirmação de nova senha deve ser do tipo texto' })
  @MinLength(6, {
    message: 'Confirmação de nova senha deve ter no mínimo 6 caracteres',
  })
  newPasswordConfirmation: string;
}
