import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { AccountEntity } from './account.entity';
import { ProfileTypeType } from '../enums/profile-type.enum';

@Entity({
  schema: 'public',
  name: 'profile',
  synchronize: false,
})
export class ProfileEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment', {
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'varchar',
    name: 'type',
  })
  type: ProfileTypeType;

  @Column({
    type: 'varchar',
    name: 'description',
  })
  description: string;

  @OneToMany(() => AccountEntity, (account) => account.profile, {
    persistence: false,
    cascade: false,
  })
  accounts: AccountEntity[];
}
