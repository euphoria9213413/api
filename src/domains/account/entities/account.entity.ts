import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ProfileEntity } from './profile.entity';
import { PasswordRecoverEntity } from './password-recover.entity';
import { SaleEntity } from 'src/domains/sale/entities/sale.entity';

@Entity({
  schema: 'public',
  name: 'account',
  synchronize: false,
})
export class AccountEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment', {
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'varchar',
    name: 'name',
  })
  name: string;

  @Column({
    type: 'varchar',
    name: 'email',
  })
  email: string;

  @Column({
    type: 'varchar',
    name: 'cpf',
  })
  cpf: string;

  @Column({
    type: 'date',
    name: 'birth',
  })
  birth: Date;

  @Column({
    type: 'varchar',
    name: 'password',
  })
  password: string;

  @Column({
    type: 'varchar',
    name: 'contact',
  })
  contact: string;

  @Column({
    type: 'boolean',
    name: 'situation',
  })
  situation: boolean;

  @ManyToOne(() => ProfileEntity, (profile) => profile.accounts, {
    persistence: false,
    cascade: false,
  })
  @Column({ type: 'integer', name: 'profileId' })
  profile: ProfileEntity;

  @OneToOne(
    () => PasswordRecoverEntity,
    (passwordRecovery) => passwordRecovery.account,
    {
      persistence: false,
      cascade: false,
    },
  )
  passwordRecovery: PasswordRecoverEntity;

  @OneToMany(() => SaleEntity, (saleEntity) => saleEntity.account, {
    persistence: false,
    cascade: false,
  })
  sales: SaleEntity;

  @CreateDateColumn({
    type: 'timestamp',
    name: 'createdAt',
    default: () => 'NOW()',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    name: 'updatedAt',
    default: () => 'NOW()',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    name: 'deletedAt',
  })
  deletedAt: Date;
}
