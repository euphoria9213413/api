import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { AccountEntity } from './account.entity';

@Entity({
  schema: 'public',
  name: 'password_recover',
  synchronize: false,
})
export class PasswordRecoverEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment', {
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'varchar',
    name: 'accessToken',
  })
  accessToken: string;

  @Column({
    type: 'timestamp',
    name: 'expirationDate',
  })
  expirationDate: Date;

  @OneToOne(() => AccountEntity, (account) => account.passwordRecovery, {
    persistence: false,
    cascade: false,
  })
  @JoinColumn()
  @Column({
    type: 'integer',
    name: 'accountId',
  })
  account: AccountEntity;

  @CreateDateColumn({
    type: 'timestamp',
    name: 'createdAt',
    default: () => 'NOW()',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    name: 'updatedAt',
    default: () => 'NOW()',
  })
  updatedAt: Date;
}
