import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { ProfileEntity } from '../entities/profile.entity';
import { ProfileDataDto } from '../dtos/profile.dto';

@Injectable()
export class ProfileRepository extends Repository<ProfileEntity> {
  constructor(private readonly dataSource: DataSource) {
    super(ProfileEntity, dataSource.createEntityManager());
  }

  async getAll(): Promise<ProfileDataDto[]> {
    return await this.find();
  }
}
