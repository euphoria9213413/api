import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { PasswordRecoverEntity } from '../entities/password-recover.entity';
import {
  PasswordRecoverDataDto,
  PasswordRecoverStoreDto,
} from '../dtos/password-recover.dto';

@Injectable()
export class PasswordRecoverRepository extends Repository<PasswordRecoverEntity> {
  constructor(private readonly dataSource: DataSource) {
    super(PasswordRecoverEntity, dataSource.createEntityManager());
  }

  async storeOrUpdate(
    data: PasswordRecoverStoreDto,
    conflictPaths: string[],
  ): Promise<void> {
    await this.upsert({ ...data } as any, {
      conflictPaths,
      skipUpdateIfNoValuesChanged: true,
    });
  }

  async findByToken(accessToken: string): Promise<PasswordRecoverDataDto> {
    return await this.findOneOrFail({
      where: { accessToken },
      relations: ['account'],
    });
  }
}
