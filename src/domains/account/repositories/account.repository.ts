import { DataSource, ILike, Repository } from 'typeorm';
import { AccountEntity } from '../entities/account.entity';
import { Injectable } from '@nestjs/common';
import { AccountDataDto, AccountIndexPaginatedDto } from '../dtos/account.dto';
import {
  AccountIndexQueryDto,
  AccountStoreDto,
  AccountEditDto,
} from '../form-validations/account.form-validation';
import { accountIndexMapper } from '../mappers/account-index.mapper';

@Injectable()
export class AccountRepository extends Repository<AccountEntity> {
  constructor(private dataSource: DataSource) {
    super(AccountEntity, dataSource.createEntityManager());
  }

  async getAllByQuery(
    query: AccountIndexQueryDto,
  ): Promise<AccountIndexPaginatedDto> {
    const page = Number(query.page);
    const perPage = Number(query.perPage);

    let filter = {} as any;

    switch (query.profileType) {
      case 'administrador':
        filter = { ...filter, profile: 1 };
        break;
      case 'cliente':
        filter = { ...filter, profile: 2 };
      default:
        break;
    }

    switch (query.situation) {
      case 'ativos':
        filter = { ...filter, situation: true };
        break;
      case 'inativos':
        filter = { ...filter, situation: false };
      default:
        break;
    }

    const nameFilter = {
      ...filter,
      name: ILike(`%${query.nameOrEmail || ''}%`),
    };
    const emailFilter = {
      ...filter,
      email: ILike(`%${query.nameOrEmail || ''}%`),
    };

    const [result, total] = await this.findAndCount({
      where: [nameFilter, emailFilter],
      relations: ['profile'],
      skip: (page - 1) * perPage,
      take: perPage,
      order: {
        name: {
          direction: 'ASC',
        },
      },
    });

    return accountIndexMapper(result, page, perPage, total);
  }

  async findById(id: number): Promise<AccountDataDto> {
    return await this.findOneOrFail({ where: { id }, relations: ['profile'] });
  }

  async store(data: AccountStoreDto): Promise<AccountDataDto> {
    return await this.save(data);
  }

  async editById(id: number, data: AccountEditDto): Promise<AccountDataDto> {
    return await this.save({
      id,
      ...data,
    });
  }

  async findByEmail(email: string): Promise<AccountDataDto> {
    return await this.findOneOrFail({
      where: { email },
      relations: ['profile'],
    });
  }

  async existsByEmail(email: string): Promise<boolean> {
    return await this.exist({ where: { email } });
  }

  async existsByCpf(cpf: string): Promise<boolean> {
    return await this.exist({ where: { cpf } });
  }

  async updateById(id: number, body: any): Promise<void> {
    await this.update({ id }, { ...body });
  }
}
