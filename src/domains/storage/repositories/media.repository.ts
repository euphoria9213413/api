import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { MediaEntity } from '../entities/media.entity';
import { MediaDataDto, StoreMediaDto } from '../dtos/media.dto';

@Injectable()
export class MediaRepository extends Repository<MediaEntity> {
  constructor(private readonly dataSource: DataSource) {
    super(MediaEntity, dataSource.createEntityManager());
  }

  async store(data: StoreMediaDto): Promise<MediaDataDto> {
    return await this.save(data);
  }
}
