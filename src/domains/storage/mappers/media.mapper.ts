import { StoreMediaDto } from '../dtos/media.dto';

export const mediaStorageMapper = (
  data: any,
  name: string,
  mimeType: string,
): StoreMediaDto => {
  const locationSplit = data.Location.split('/');

  return {
    name,
    uniqueName: data.key,
    directory: `${locationSplit[0]}//${locationSplit[2]}`,
    mimeType,
    url: data.Location,
  };
};
