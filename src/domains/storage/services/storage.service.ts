import { Injectable } from '@nestjs/common';
import { S3 } from 'aws-sdk';
import env from 'src/config/env';
import { StoreMediaDto } from '../dtos/media.dto';
import { mediaStorageMapper } from '../mappers/media.mapper';
import { UploadFileException } from '../exceptions/upload-file.exception';

@Injectable()
export class StorageService {
  async upload(file: Express.Multer.File): Promise<StoreMediaDto> {
    const { originalname, buffer, mimetype } = file;

    const params = {
      Bucket: env.aws.bucket.banner_name,
      Key: `${Date.now().toString()}-${originalname}`,
      Body: buffer,
      ContentType: 'image/png',
    };

    const media = await new Promise((resolve, reject) => {
      new S3().upload(params, (err, data) => {
        if (err) {
          reject(err.message);
          throw new UploadFileException();
        }

        resolve(data);
      });
    });

    return mediaStorageMapper(media, originalname, mimetype);
  }

  async getSecuritFile(fileName: string): Promise<any> {
    const params = {
      Bucket: env.aws.bucket.document_name,
      Key: fileName,
    };

    try {
      const response = await new S3().getObject(params).promise();

      return response.Body;
    } catch (error) {
      return { ...error };
    }
  }
}
