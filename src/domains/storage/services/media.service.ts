import { Injectable } from '@nestjs/common';
import { MediaRepository } from '../repositories/media.repository';
import { MediaDataDto, StoreMediaDto } from '../dtos/media.dto';

@Injectable()
export class MediaService {
  constructor(private readonly mediaRepository: MediaRepository) {}

  async store(data: StoreMediaDto): Promise<MediaDataDto> {
    return await this.mediaRepository.store(data);
  }
}
