export type MediaDataDto = {
  id: number;
  name: string;
  uniqueName: string;
  directory: string;
  mimeType: string;
  url?: string;
  createdAt: Date;
  updatedAt: Date;
};

export type StoreMediaDto = Omit<
  MediaDataDto,
  'id' | 'createdAt' | 'updatedAt'
>;
