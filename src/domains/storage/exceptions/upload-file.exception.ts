import { HttpException, HttpStatus } from '@nestjs/common';

export class UploadFileException extends HttpException {
  constructor() {
    super('Falha ao salvar arquivo', HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
