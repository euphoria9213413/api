import { StoreEntity } from 'src/domains/store/entities/store.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({
  schema: 'public',
  name: 'media',
  synchronize: false,
})
export class MediaEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment', {
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'varchar',
    name: 'name',
  })
  name: string;

  @Column({
    type: 'varchar',
    name: 'uniqueName',
  })
  uniqueName: string;

  @Column({
    type: 'varchar',
    name: 'directory',
  })
  directory: string;

  @Column({
    type: 'varchar',
    name: 'mimeType',
  })
  mimeType: string;

  @Column({
    type: 'varchar',
    name: 'url',
  })
  url: string;

  @OneToOne(() => StoreEntity, (store) => store.desktopMedia, {
    persistence: false,
    cascade: false,
  })
  desktopStore: StoreEntity;

  @OneToOne(() => StoreEntity, (store) => store.mobileMedia, {
    persistence: false,
    cascade: false,
  })
  mobileStore: StoreEntity;

  @CreateDateColumn({
    type: 'timestamp',
    name: 'createdAt',
    default: () => 'NOW()',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    name: 'updatedAt',
    default: () => 'NOW()',
  })
  updatedAt: Date;
}
