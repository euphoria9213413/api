import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MediaEntity } from './entities/media.entity';
import { MediaService } from './services/media.service';
import { MediaRepository } from './repositories/media.repository';

@Module({
  imports: [TypeOrmModule.forFeature([MediaEntity])],
  controllers: [],
  providers: [MediaService, MediaRepository],
  exports: [MediaService],
})
export class MediaModule {}
