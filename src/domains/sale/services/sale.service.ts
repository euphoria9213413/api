import { HttpStatus, Injectable } from '@nestjs/common';
import { SaleRepository } from '../repositories/sale.repository';
import {
  SaleExportQueryDto,
  SaleIndexMineQueryDto,
  SaleIndexOrderByTokenQueryDto,
  SaleIndexQueryDto,
  SaleStatusUpdateDto,
  SaleStoreFormDto,
  SaleUpdateFormDto,
} from '../form-validations/sale.form-validation';
import {
  SaleDataDto,
  SaleIndexPaginatedDto,
  SalePixCopyPaste,
} from '../dtos/sale.dto';
import { NoSaleFoundToExportException } from 'src/domains/store/exceptions/no-sale-found-to-export.exception';
import {
  saleStoreMapper,
  saleStorePayedMapper,
  saleStoreUpdateMapper,
} from '../mappers/sale-store.mapper';
import { MailService } from 'src/domains/mail/services/mail.service';
import { saleEmailTemplate } from 'src/domains/mail/templates/sale-email.template';
import { PtBrDataFormatterUtil } from 'src/utils/pt-br-data-formater.util';
import { PaymentService } from 'src/domains/payment/services/payment.service';
import { PaymentStatusEnum, PaymentTypeEnum } from 'src/domains/payment/enums';
import { salePackEmailTemplate } from 'src/domains/mail/templates/sale-pack-email.template';
import { FeePolicyEnum } from 'src/domains/store/enums/fee-policy.enum';
import { CreditCardPaymentService } from 'src/domains/payment/services/credit-card-payment';
import axios from 'axios';
import { CreditCardPaymentFailedException } from '../exception/credit-card-payment-failed.exception';
import { ResponseMessageEnum } from '../enums/response-message.enum';
import { CreditCardPaymentDeclinedException } from '../exception/credit-card-payment-declined.exception copy';
import { SendCreditCartPaymentDto } from 'src/domains/payment/dtos';
import { SecretUtil } from 'src/utils/secret.util';
import { createWriteStream } from 'fs';
import { Readable, Transform, pipeline } from 'stream';
import { promisify } from 'util';

@Injectable()
export class SaleService {
  public FILE_PATH = './files/AEA-Vendas.csv';

  constructor(
    private readonly saleRepository: SaleRepository,
    private readonly mailService: MailService,
    private readonly paymentService: PaymentService,
    private readonly creditCardPaymentService: CreditCardPaymentService,
  ) {}

  async index(storeId: number, query: SaleIndexQueryDto): Promise<any> {
    return await this.saleRepository.getAllByQuery(storeId, query);
  }

  async export(storeId: number, query: SaleExportQueryDto): Promise<any> {
    const repository = this.saleRepository;

    const pipelineAsync = promisify(pipeline);

    query.page = '1';
    query.perPage = '20000';

    const readableStream = new Readable({
      objectMode: true,
      async read() {
        const data = await repository.exportAllByQuery(storeId, query);

        console.log(data.length, query.page);

        if (!data.length) {
          if (query.page === '1') throw new NoSaleFoundToExportException();
          this.push(null);
          return;
        }

        data.forEach((row, index) => {
          if (query.page === '1' && index === 0) {
            this.push(`${Object.keys(row).join(',')}\n`);
          }

          const value = Object.values(row).join(',');

          this.push(`${value}\n`);
        });

        query.page = String(Number(query.page) + 1);
      },
    });

    const writableMapToCSV = new Transform({
      transform(chunk, encoding, cb) {
        cb(null, chunk);
      },
    });

    await pipelineAsync(
      readableStream,
      writableMapToCSV,
      createWriteStream(this.FILE_PATH),
    );
  }

  async indexMine(
    query: SaleIndexMineQueryDto,
  ): Promise<SaleIndexPaginatedDto> {
    return await this.saleRepository.getMineByQuery(query);
  }

  async indexOrderByToken(
    accountId: number,
    query: SaleIndexOrderByTokenQueryDto,
  ): Promise<SaleIndexPaginatedDto> {
    return await this.saleRepository.getOrderByTokenAndQuery(accountId, query);
  }

  async store(
    accountId: number,
    data: SaleStoreFormDto,
  ): Promise<SalePixCopyPaste | void> {
    const savedSales = await this.saleRepository.manager.transaction(
      async () => {
        const sales = saleStoreMapper(accountId, data);

        const savedSales = await this.saleRepository.store(sales);
        const findSavedSales = await this.saleRepository.findAllById(
          savedSales.map((saved) => saved.id),
        );

        const amount = findSavedSales.reduce((acc, curr) => {
          acc += Number(curr.product.unitaryValue);
          return acc;
        }, 0);

        if (PaymentTypeEnum.credit_card === data.paymentType) {
          await this.sendCreditCardPayment(findSavedSales, data, amount);

          return;
        }

        const paymentData = await this.paymentService.generatePixPayment(
          String(amount),
        );

        const payment = await this.paymentService.store({
          type: PaymentTypeEnum[data.paymentType],
          status: PaymentStatusEnum.ATIVA,
          transactionId: paymentData.txid,
          value: String(amount),
          copyPaste: paymentData.pixCopyPasteValue,
          expiration: paymentData.expiration,
        });

        const savedSalesUpdate = saleStoreUpdateMapper(
          accountId,
          findSavedSales,
          payment.id,
        );

        await this.saleRepository.store(savedSalesUpdate);

        return {
          pixCopyPasteValue: paymentData.pixCopyPasteValue,
          expiration: paymentData.expiration,
        };
      },
    );

    let date = null;
    if (savedSales?.expiration) {
      date = PtBrDataFormatterUtil.dateAndHourFormater(savedSales.expiration);
    }

    return {
      pixCopyPasteValue: savedSales?.pixCopyPasteValue,
      expirationDate: date?.date,
      expirationHour: date?.hour,
    };
  }

  private async sendCreditCardPayment(
    sales: SaleDataDto[],
    data: SaleStoreFormDto,
    amount: number,
  ): Promise<any> {
    const ipAddress = await axios.get<{ ip: string }>(
      'https://api.ipify.org/?format=json',
    );

    const isTaxPass =
      FeePolicyEnum.repassar === sales[0].product.store.feePolicy;

    const calculatedAmount = isTaxPass
      ? this.creditCardPaymentService.getCalculatedAmount(
          amount,
          data.installment,
        )
      : String(amount);

    const paymentData: SendCreditCartPaymentDto = {
      ipAddress: ipAddress.data.ip,
      cardNumber: SecretUtil.decrypt(data.cardNumber),
      expirationDate: SecretUtil.decrypt(data.expirationDate),
      secretNumber: SecretUtil.decrypt(data.secretNumber),
      referenceNum: `${sales[0].id}`,
      installment: data.installment,
      amount: calculatedAmount,
    };

    const payment = await this.creditCardPaymentService.sendPayment(
      paymentData,
    );

    if (HttpStatus.OK !== payment.status) {
      const salesId = sales.map((sale) => sale.id);
      this.saleRepository.deleteById(salesId);

      throw new CreditCardPaymentFailedException(payment.status);
    }

    if (
      ResponseMessageEnum.captured ===
      payment.data['transaction-response'].responseMessage
    ) {
      const storedPayment = await this.paymentService.store({
        type: PaymentTypeEnum[data.paymentType],
        status: PaymentStatusEnum.CONCLUIDA,
        transactionId: String(
          payment.data['transaction-response'].transactionID,
        ),
        value: calculatedAmount,
        orderId: payment.data['transaction-response'].orderID,
      });

      await this.sendSaleEmail(sales, storedPayment.id);

      return;
    }

    if (
      !payment.data['transaction-response'].responseMessage ||
      ResponseMessageEnum.declined ===
        payment.data['transaction-response'].responseMessage
    ) {
      const salesId = sales.map((sale) => sale.id);
      this.saleRepository.deleteById(salesId);
      throw new CreditCardPaymentDeclinedException();
    }

    const salesId = sales.map((sale) => sale.id);
    this.saleRepository.deleteById(salesId);

    throw new CreditCardPaymentFailedException();
  }

  public async sendSaleEmail(
    sales: SaleDataDto[],
    paymentId: number,
  ): Promise<void> {
    const savedSalesUpdate = saleStorePayedMapper(
      sales[0].account.id,
      sales,
      paymentId,
    );

    const stored = await this.saleRepository.store(savedSalesUpdate);

    const findsavedSales = await this.saleRepository.findAllById(
      stored.map((saved) => saved.id),
    );

    const purchaseDate = PtBrDataFormatterUtil.dateAndHourPeriodFormater(
      findsavedSales[0].createdAt,
    );

    const packEmailTemplate = salePackEmailTemplate(
      findsavedSales[0].account.name,
      `${purchaseDate.date} ${purchaseDate.hour}`,
      findsavedSales[0].product.store.name,
      findsavedSales,
    );

    this.mailService.sendMailResend(
      findsavedSales[0].account.email,
      packEmailTemplate,
      `Confirmação de compra - ${findsavedSales[0].product.store.name}`,
    );
  }

  async resendEmail(id: number): Promise<void> {
    const sale = await this.saleRepository.findById(id);

    const purchaseDate = PtBrDataFormatterUtil.dateAndHourPeriodFormater(
      sale.createdAt,
    );

    const emailTemplate = saleEmailTemplate(
      sale.buyerName,
      sale.luckyNumber,
      `${purchaseDate.date} ${purchaseDate.hour}`,
      sale.product.store.name,
      sale.graduate.name,
      sale.product.name,
      PtBrDataFormatterUtil.moneyFormat(Number(sale.price)),
    );

    this.mailService.sendMailResend(
      sale.account.email,
      emailTemplate,
      `Confirmação de compra - ${sale.luckyNumber} - ${sale.product.store.name}`,
      sale.buyerEmail,
    );
  }

  async statusUpdate(id: number, data: SaleStatusUpdateDto): Promise<void> {
    const sale = await this.saleRepository.findById(id);

    await this.paymentService.statusUpdate(sale.payment.id, data);
  }

  async edit(id: number, data: SaleUpdateFormDto): Promise<void> {
    await this.saleRepository.updateById(id, data);
  }
}
