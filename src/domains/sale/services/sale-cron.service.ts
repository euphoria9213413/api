import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import env from 'src/config/env';
import { PaymentService } from 'src/domains/payment/services/payment.service';
import * as lodash from 'lodash';
import { PaymentStatusEnum } from 'src/domains/payment/enums';
import { SaleService } from './sale.service';

@Injectable()
export class SaleCronService {
  constructor(
    private readonly paymentService: PaymentService,
    private readonly saleService: SaleService,
  ) {}

  @Cron(env.cron.payment_status)
  async validatePaymentExpiration(): Promise<any> {
    await this.paymentService.updateAllStatusWhenExpired();
  }

  @Cron(env.cron.consult_payment)
  async consultPayment(): Promise<any> {
    const pendingPayments = await this.paymentService.getPendingPayments();

    if (!pendingPayments.length) return;

    const chunkPendingPayments = lodash.chunk(pendingPayments, 10);

    for (const chunkedPendingPayments of chunkPendingPayments) {
      const token = await this.paymentService.getToken();

      await Promise.all(
        chunkedPendingPayments.map(async (pendingPayment) => {
          const paymentStatus = await this.paymentService.consultPayment(
            pendingPayment.transactionId,
            token,
          );

          if (PaymentStatusEnum.ERRO_403 === paymentStatus.status) return;

          if (PaymentStatusEnum.ATIVA !== paymentStatus.status)
            this.paymentService.update(pendingPayment.id, paymentStatus.status);

          if (PaymentStatusEnum.CONCLUIDA !== paymentStatus.status) return;

          await this.saleService.sendSaleEmail(
            pendingPayment.sales,
            pendingPayment.id,
          );
        }),
      );
    }
  }
}
