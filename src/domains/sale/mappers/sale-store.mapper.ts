import { SaleDataDto, SaleStoreDto, SaleUpdateDto } from '../dtos/sale.dto';
import { SaleStoreFormDto } from '../form-validations/sale.form-validation';

export const saleStoreMapper = (
  accountId: number,
  data: SaleStoreFormDto,
): SaleStoreDto[] => {
  return data.sales.map((sale) => {
    return {
      buyerName: sale.buyerName,
      buyerContact: sale.buyerContact,
      buyerEmail: sale.buyerEmail,
      product: { id: sale.productId },
      account: { id: accountId },
      graduate: { id: sale.graduateId },
    };
  });
};

export const saleStoreUpdateMapper = (
  accountId: number,
  sales: SaleDataDto[],
  paymentId: number,
): SaleUpdateDto[] => {
  return sales.map((sale): SaleUpdateDto => {
    return {
      id: sale.id,
      buyerName: sale.buyerName,
      buyerContact: sale.buyerContact,
      buyerEmail: sale.buyerEmail,
      price: sale.product.unitaryValue,
      product: { id: sale.product.id },
      account: { id: accountId },
      graduate: { id: sale.graduate.id },
      payment: { id: paymentId },
    };
  });
};

export const saleStorePayedMapper = (
  accountId: number,
  sales: SaleDataDto[],
  paymentId: number,
): SaleUpdateDto[] => {
  return sales.map((sale): SaleUpdateDto => {
    return {
      id: sale.id,
      luckyNumber: `${sale.product.store.nickname} - ${sale.id}`,
      buyerName: sale.buyerName,
      buyerContact: sale.buyerContact,
      buyerEmail: sale.buyerEmail,
      price: sale.product.unitaryValue,
      product: { id: sale.product.id },
      account: { id: accountId },
      graduate: { id: sale.graduate.id },
      payment: { id: paymentId },
    };
  });
};
