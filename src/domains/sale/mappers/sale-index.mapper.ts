import { PtBrDataFormatterUtil } from 'src/utils/pt-br-data-formater.util';
import {
  SaleDataDto,
  SaleIndexDto,
  SaleIndexPaginatedDto,
} from '../dtos/sale.dto';

export const saleIndexMapper = (
  data: SaleDataDto[],
  page: number,
  perPage: number,
  total: number,
): SaleIndexPaginatedDto => {
  const sales = data.map((sale): SaleIndexDto => {
    const dateAndHourFormated = PtBrDataFormatterUtil.dateAndHourPeriodFormater(
      sale.createdAt,
    );

    const expiration = sale.payment.expiration
      ? PtBrDataFormatterUtil.dateAndHourFormater(sale.payment.expiration)
      : null;

    return {
      id: sale.id,
      luckyNumber: sale.luckyNumber,
      graduateCode: sale.graduate.code,
      graduateName: sale.graduate.name,
      graduateClass: sale.graduate.class,
      buyerName: sale.buyerName,
      buyerContact: sale.buyerContact,
      buyerEmail: sale.buyerEmail,
      storeName: sale.graduate.store.name,
      storeNickname: sale.graduate.store.nickname,
      price: sale.price,
      graduateMargin: sale.product.graduateMargin,
      date: dateAndHourFormated.date,
      hour: dateAndHourFormated.hour,
      productName: sale.product.name,
      paymentStatus: sale.payment.status,
      paymentType: sale.payment.type,
      paymentCopyPaste: sale.payment.copyPaste,
      expirationDate: expiration?.date,
      expirationHour: expiration?.hour,
    };
  });

  return {
    data: sales,
    page,
    perPage,
    lastPage: Math.ceil(total / perPage),
    total,
  };
};
