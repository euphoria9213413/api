import { HttpException, HttpStatus } from '@nestjs/common';

export class CreditCardPaymentFailedException extends HttpException {
  constructor(status?: number) {
    super(
      'Falha ao processar o pagamento. Tente novamente',
      status ?? HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }
}
