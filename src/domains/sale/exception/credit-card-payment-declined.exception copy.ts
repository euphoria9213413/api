import { HttpException, HttpStatus } from '@nestjs/common';

export class CreditCardPaymentDeclinedException extends HttpException {
  constructor() {
    super(
      'Pagamento recusado. Revise os dados do cartão e ente novamente',
      HttpStatus.BAD_REQUEST,
    );
  }
}
