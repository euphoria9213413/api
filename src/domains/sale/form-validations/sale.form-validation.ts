import { IsEmail, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class SaleIndexQueryDto {
  @IsOptional()
  graduateName?: string;

  @IsOptional()
  buyerName?: string;

  @IsOptional()
  accountName?: string;

  @IsNotEmpty({ message: 'Página deve ser informada' })
  page: string;

  @IsNotEmpty({ message: 'Quantidade por página deve ser informada' })
  perPage: string;
}

export class SaleIndexMineQueryDto {
  @IsOptional()
  graduateCpf?: string;

  @IsOptional()
  storeName?: string;

  @IsNotEmpty({ message: 'Página deve ser informada' })
  page: string;

  @IsNotEmpty({ message: 'Quantidade por página deve ser informada' })
  perPage: string;
}

export class SaleIndexOrderByTokenQueryDto {
  @IsOptional()
  buyerNameOrBuyerEmailOrGraduateName?: string;

  @IsNotEmpty({ message: 'Página deve ser informada' })
  page: string;

  @IsNotEmpty({ message: 'Quantidade por página deve ser informada' })
  perPage: string;
}

export class SaleExportQueryDto extends SaleIndexQueryDto {}

class CheckoutFormDataDto {
  @IsNotEmpty({ message: 'Id do produto ser informado' })
  productId: number;

  @IsNotEmpty({ message: 'Id do formando deve ser informado' })
  graduateId: number;

  @IsNotEmpty({ message: 'Nome do formando deve ser informado' })
  @IsString({ message: 'Nome do formando  deve ser do tipo texto' })
  graduateName: string;

  @IsNotEmpty({ message: 'Nome do comprador deve ser informado' })
  @IsString({ message: 'Nome do comprador  deve ser do tipo texto' })
  buyerName: string;

  @IsNotEmpty({ message: 'E-mail do comprador deve ser informado' })
  @IsString({ message: 'E-mail do comprador  deve ser do tipo texto' })
  buyerEmail: string;

  @IsOptional()
  @IsEmail({}, { message: 'E-mail inválido' })
  buyerContact?: string;
}

export class SaleStoreFormDto {
  @IsNotEmpty({ message: 'Vendas deve ser informada' })
  sales: CheckoutFormDataDto[];

  @IsNotEmpty({ message: 'Nome do comprador no pagamento deve ser informada' })
  @IsString({ message: 'Nome do comprador deve ser do tipo texto' })
  paymentBuyerName: string;

  @IsNotEmpty({ message: 'Telefone no pagamento deve ser informada' })
  @IsString({ message: 'Telefone deve ser do tipo texto' })
  paymentContact: string;

  @IsNotEmpty({ message: 'CPF no pagamento deve ser informada' })
  @IsString({ message: 'CPF deve ser do tipo texto' })
  paymentCpf: string;

  @IsOptional()
  @IsString({ message: 'Data de nascimento do pagador deve ser do tipo texto' })
  paymentBirth: string;

  @IsString({ message: 'Tipo do pagamento deve ser do tipo texto' })
  paymentType: 'pix' | 'credit_card';

  @IsOptional()
  cardNumber: string;

  @IsOptional()
  expirationDate: string;

  @IsOptional()
  secretNumber: string;

  @IsOptional()
  installment: number;
}

export class SaleUpdateFormDto {
  @IsOptional()
  graduate: number;

  @IsOptional()
  @IsString({ message: 'Nome do comprador deve ser do tipo texto' })
  buyerName: string;

  @IsOptional()
  @IsString({ message: 'Telefone deve ser do tipo texto' })
  buyerContact: string;

  @IsOptional()
  @IsString({ message: 'E-mail do comprador  deve ser do tipo texto' })
  buyerEmail: string;
}

export class SaleStatusUpdateDto {
  @IsNotEmpty({ message: 'Status deve ser informado' })
  status: string;
}
