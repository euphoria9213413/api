import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SaleEntity } from './entities/sale.entity';
import { SaleRepository } from './repositories/sale.repository';
import { SaleService } from './services/sale.service';
import { SaleController } from './controllers/sale.controller';
import { AccountModule } from '../account/account.module';
import { MailModule } from '../mail/mail.module';
import { PaymentModule } from '../payment/payment.module';
import { SaleCronService } from './services/sale-cron.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([SaleEntity]),
    AccountModule,
    MailModule,
    PaymentModule,
  ],
  controllers: [SaleController],
  providers: [SaleService, SaleCronService, SaleRepository],
})
export class SaleModule {}
