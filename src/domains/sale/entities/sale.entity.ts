import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { GraduateEntity } from '../../store/entities/graduate.entity';
import { ProductEntity } from 'src/domains/store/entities/product.entity';
import { AccountEntity } from 'src/domains/account/entities/account.entity';
import { PaymentEntity } from 'src/domains/payment/entities/payment.entity';

@Entity({
  schema: 'public',
  name: 'sale',
  synchronize: false,
})
export class SaleEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment', {
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'varchar',
    name: 'luckyNumber',
  })
  luckyNumber: string;

  @Column({
    type: 'varchar',
    name: 'buyerName',
  })
  buyerName: string;

  @Column({
    type: 'varchar',
    name: 'buyerContact',
  })
  buyerContact: string;

  @Column({
    type: 'varchar',
    name: 'buyerEmail',
  })
  buyerEmail: string;

  @Column({
    type: 'numeric',
    name: 'price',
  })
  price: string;

  @ManyToOne(() => ProductEntity, (product) => product.sales, {
    persistence: false,
    cascade: false,
  })
  @Column({
    type: 'integer',
    name: 'productId',
  })
  product: ProductEntity;

  @ManyToOne(() => GraduateEntity, (graduate) => graduate.sales, {
    persistence: false,
    cascade: false,
  })
  @Column({
    type: 'integer',
    name: 'graduateId',
  })
  graduate: GraduateEntity;

  @ManyToOne(() => AccountEntity, (account) => account.sales, {
    persistence: false,
    cascade: false,
  })
  @Column({
    type: 'integer',
    name: 'accountId',
  })
  account: AccountEntity;

  @ManyToOne(() => PaymentEntity, (payment) => payment.sales, {
    persistence: false,
    cascade: false,
  })
  @Column({
    type: 'integer',
    name: 'paymentId',
  })
  payment: PaymentEntity;

  @CreateDateColumn({
    type: 'timestamp',
    name: 'createdAt',
    default: () => 'NOW()',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    name: 'updatedAt',
    default: () => 'NOW()',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    name: 'deletedAt',
  })
  deletedAt: Date;
}
