import { Injectable } from '@nestjs/common';
import { Brackets, DataSource, In, Repository } from 'typeorm';
import { SaleEntity } from '../entities/sale.entity';
import {
  SaleExportQueryDto,
  SaleIndexMineQueryDto,
  SaleIndexOrderByTokenQueryDto,
  SaleIndexQueryDto,
} from '../form-validations/sale.form-validation';
import {
  SaleDataDto,
  SaleIndexPaginatedDto,
  SaleStoreDto,
} from '../dtos/sale.dto';
import { saleIndexMapper } from '../mappers/sale-index.mapper';

@Injectable()
export class SaleRepository extends Repository<SaleEntity> {
  constructor(private readonly dataSource: DataSource) {
    super(SaleEntity, dataSource.createEntityManager());
  }

  async getAllByQuery(
    storeId: number,
    query: SaleIndexQueryDto,
  ): Promise<SaleIndexPaginatedDto> {
    const page = Number(query.page);
    const perPage = Number(query.perPage);

    const queryBuilder = this.createQueryBuilder('sale')
      .innerJoin('sale.account', 'account')
      .innerJoin('sale.product', 'product')
      .innerJoin('sale.graduate', 'graduate')
      .innerJoin('sale.payment', 'payment')
      .innerJoin('graduate.store', 'store')
      .select('sale')
      .addSelect('account')
      .addSelect('product')
      .addSelect('graduate')
      .addSelect('payment')
      .addSelect('store')
      .where('store.id = :storeId', { storeId })
      .andWhere('sale.buyerName ilike :buyerName', {
        buyerName: `%${query.buyerName || ''}%`,
      })
      .andWhere('graduate.name ilike :graduateName', {
        graduateName: `%${query.graduateName || ''}%`,
      })
      .andWhere('account.name ilike :accountName', {
        accountName: `%${query.accountName || ''}%`,
      })
      .orderBy('sale.createdAt', 'ASC');

    const [result, total] = await queryBuilder
      .skip((page - 1) * perPage)
      .take(perPage)
      .getManyAndCount();

    return saleIndexMapper(result, page, perPage, total);
  }

  async exportAllByQuery(
    storeId: number,
    query: SaleExportQueryDto,
  ): Promise<SaleDataDto[]> {
    const page = Number(query.page);
    const perPage = Number(query.perPage);

    const queryBuilder = this.createQueryBuilder('sale')
      .innerJoin('sale.account', 'account')
      .innerJoin('sale.product', 'product')
      .innerJoin('sale.graduate', 'graduate')
      .innerJoin('sale.payment', 'payment')
      .innerJoin('graduate.store', 'store')
      .select('"sale"."luckyNumber"', 'Número da sorte')
      .addSelect('"graduate"."code"', 'Código formando')
      .addSelect('"graduate"."name"', 'Nome formando')
      .addSelect('"sale"."buyerName"', 'Nome comprador')
      .addSelect('"sale"."buyerContact"', 'Telefone comprador')
      .addSelect(
        "lpad(extract(day from sale.createdAt AT TIME ZONE 'UTC' AT TIME ZONE 'america/sao_paulo')::varchar, 2, '0') || '/' || lpad(extract(month from sale.createdAt AT TIME ZONE 'UTC' AT TIME ZONE 'america/sao_paulo')::varchar, 2, '0') || '/' || extract(year from sale.createdAt AT TIME ZONE 'UTC' AT TIME ZONE 'america/sao_paulo')",
        'Data',
      )
      .addSelect(
        "lpad(extract(hour from sale.createdAt AT TIME ZONE 'UTC' AT TIME ZONE 'america/sao_paulo')::varchar, 2, '0') || ':' || lpad(extract(minute from sale.createdAt AT TIME ZONE 'UTC' AT TIME ZONE 'america/sao_paulo')::varchar, 2, '0')",
        'Hora',
      )
      .addSelect('"sale"."buyerEmail"', 'E-mail comprador')
      .addSelect('"store"."name"', 'Nome loja')
      .addSelect('"graduate"."class"', 'Centro de controle')
      .addSelect('"account"."name"', 'Nome usuário')
      .addSelect('"account"."cpf"', 'CPF usuário')
      .addSelect('"product"."name"', 'Produto')
      .addSelect("concat('R$ ', sale.price)", 'Valor')
      .addSelect('"payment"."status"', 'Status')
      .addSelect(
        "case when payment.type = 'pix' then 'Pix' else 'Cartão de crédito' end",
        'Forma de pagamento',
      )
      .addSelect('"payment"."transactionId"', 'ID da transação')
      .addSelect("concat('R$ ', payment.value)", 'Valor pago')
      .where('store.id = :storeId', { storeId })
      .andWhere('sale.buyerName ilike :buyerName', {
        buyerName: `%${query.buyerName || ''}%`,
      })
      .andWhere('graduate.name ilike :graduateName', {
        graduateName: `%${query.graduateName || ''}%`,
      })
      .andWhere('account.name ilike :accountName', {
        accountName: `%${query.accountName || ''}%`,
      })
      .orderBy('sale.createdAt', 'ASC')
      .limit(perPage)
      .offset((page - 1) * perPage)
      .getRawMany();

    return await queryBuilder;
  }

  async store(data: SaleStoreDto[]): Promise<SaleDataDto[]> {
    return await this.save(data);
  }

  async findAllById(ids: number[]): Promise<SaleDataDto[]> {
    return await this.find({
      where: { id: In(ids) },
      relations: ['product.store', 'graduate', 'account'],
    });
  }

  async findById(id: number): Promise<SaleDataDto> {
    return await this.findOne({
      where: { id },
      relations: ['product.store', 'graduate', 'account', 'payment'],
    });
  }

  async getMineByQuery(
    query: SaleIndexMineQueryDto,
  ): Promise<SaleIndexPaginatedDto> {
    const page = Number(query.page);
    const perPage = Number(query.perPage);

    const queryBuilder = this.createQueryBuilder('sale')
      .innerJoin('sale.graduate', 'graduate')
      .innerJoin('sale.product', 'product')
      .innerJoin('sale.payment', 'payment')
      .innerJoin('graduate.store', 'store')
      .select('sale')
      .addSelect('graduate')
      .addSelect('store')
      .addSelect('product')
      .addSelect('payment')
      .where('graduate.cpf = :graduateCpf', {
        graduateCpf: query.graduateCpf || 'null',
      })
      .andWhere('graduate.situation = :situation', {
        situation: true,
      })
      .andWhere('store.name ilike :storeName', {
        storeName: `%${query.storeName || ''}%`,
      })
      .andWhere('payment.status = :approved', {
        approved: 'concluida',
      })
      .orderBy('sale.createdAt', 'DESC');

    const [result, total] = await queryBuilder
      .skip((page - 1) * perPage)
      .take(perPage)
      .getManyAndCount();

    return saleIndexMapper(result, page, perPage, total);
  }

  async getOrderByTokenAndQuery(
    accountId: number,
    query: SaleIndexOrderByTokenQueryDto,
  ): Promise<SaleIndexPaginatedDto> {
    const page = Number(query.page);
    const perPage = Number(query.perPage);

    const queryBuilder = this.createQueryBuilder('sale')
      .innerJoin('sale.graduate', 'graduate')
      .innerJoin('sale.product', 'product')
      .innerJoin('graduate.store', 'store')
      .innerJoin('sale.account', 'account')
      .innerJoin('sale.payment', 'payment')
      .select('sale')
      .addSelect('graduate')
      .addSelect('product')
      .addSelect('store')
      .addSelect('account')
      .addSelect('payment')
      .where('account.id = :accountId', {
        accountId: accountId,
      })
      .andWhere(
        new Brackets((db) => {
          db.where('sale.buyerName ilike :buyerName', {
            buyerName: `%${query.buyerNameOrBuyerEmailOrGraduateName || ''}%`,
          })
            .orWhere('sale.buyerEmail ilike :buyerEmail', {
              buyerEmail: `%${
                query.buyerNameOrBuyerEmailOrGraduateName || ''
              }%`,
            })
            .orWhere('graduate.name ilike :name', {
              name: `%${query.buyerNameOrBuyerEmailOrGraduateName || ''}%`,
            });
        }),
      )
      .orderBy('sale.createdAt', 'DESC');

    const [result, total] = await queryBuilder
      .skip((page - 1) * perPage)
      .take(perPage)
      .getManyAndCount();

    return saleIndexMapper(result, page, perPage, total);
  }

  async deleteById(ids: Array<number>): Promise<void> {
    await this.delete(ids);
  }

  async updateById(id: number, body: any): Promise<void> {
    await this.update({ id }, { ...body });
  }
}
