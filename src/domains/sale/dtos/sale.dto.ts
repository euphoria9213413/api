import { AccountDataDto } from 'src/domains/account/dtos/account.dto';
import { PaymentDataDto } from 'src/domains/payment/dtos';
import { GraduateDataDto } from 'src/domains/store/dtos/graduate.dto';
import { ProductDataDto } from 'src/domains/store/dtos/product.dto';

export type SaleDataDto = {
  id: number;
  luckyNumber?: string;
  buyerName: string;
  buyerContact: string;
  buyerEmail: string;
  price: string;
  product: ProductDataDto;
  account: AccountDataDto;
  graduate: GraduateDataDto;
  payment?: PaymentDataDto;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
};

export type SaleIndexDto = {
  id: number;
  luckyNumber: string;
  graduateCode: string;
  graduateName: string;
  graduateClass: string;
  buyerName: string;
  buyerContact: string;
  buyerEmail: string;
  storeName: string;
  storeNickname: string;
  price: string;
  graduateMargin: string;
  date: string;
  hour: string;
  productName: string;
  paymentStatus: string;
  paymentType: string;
  paymentCopyPaste?: string;
  expirationDate?: string;
  expirationHour?: string;
};

export type SaleIndexPaginatedDto = {
  data: SaleIndexDto[];
  page: number;
  perPage: number;
  lastPage: number;
  total: number;
};

export type SaleExportDto = {
  'Número da sorte': string;
  'Código formando': string;
  'Nome formando': string;
  'Nome comprador': string;
  'Telefone comprador': string;
  Data: string;
  Hora: string;
  'E-mail comprador': string;
  'Nome loja': string;
  'Centro de controle': string;
  'Nome usuário': string;
  'CPF usuário': string;
  Produto: string;
  Valor: string;
};

export type SaleStoreDto = {
  id?: number;
  product: Pick<ProductDataDto, 'id'>;
  account: Pick<AccountDataDto, 'id'>;
  graduate: Pick<GraduateDataDto, 'id'>;
} & Omit<
  SaleDataDto,
  | 'id'
  | 'price'
  | 'product'
  | 'account'
  | 'graduate'
  | 'payment'
  | 'createdAt'
  | 'updatedAt'
  | 'deletedAt'
>;

export type SaleUpdateDto = {
  id?: number;
  product: Pick<ProductDataDto, 'id'>;
  account: Pick<AccountDataDto, 'id'>;
  graduate: Pick<GraduateDataDto, 'id'>;
  payment: Pick<PaymentDataDto, 'id'>;
} & Omit<
  SaleDataDto,
  | 'id'
  | 'product'
  | 'account'
  | 'graduate'
  | 'payment'
  | 'createdAt'
  | 'updatedAt'
  | 'deletedAt'
>;

export type SalePixCopyPaste = {
  pixCopyPasteValue?: string;
  expirationDate: string;
  expirationHour: string;
};
