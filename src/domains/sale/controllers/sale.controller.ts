import {
  Body,
  Controller,
  Get,
  Header,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from 'src/config/guards/auth.guard';
import { RoleAdminGuard } from 'src/config/guards/role-admin.guard';
import { SaleService } from '../services/sale.service';
import { SaleIndexPaginatedDto, SalePixCopyPaste } from '../dtos/sale.dto';
import {
  SaleExportQueryDto,
  SaleIndexMineQueryDto,
  SaleIndexOrderByTokenQueryDto,
  SaleIndexQueryDto,
  SaleStatusUpdateDto,
  SaleStoreFormDto,
  SaleUpdateFormDto,
} from '../form-validations/sale.form-validation';
import { Response } from 'express';
import { createReadStream } from 'fs';

@Controller('sale')
export class SaleController {
  constructor(private readonly saleService: SaleService) {}

  @Get(':storeId/index')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.OK)
  async index(
    @Param('storeId') storeId: string,
    @Query() query: SaleIndexQueryDto,
  ): Promise<SaleIndexPaginatedDto> {
    return await this.saleService.index(Number(storeId), query);
  }

  @Get(':storeId/export')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @Header('Content-Type', 'text/csv')
  @HttpCode(HttpStatus.OK)
  async export(
    @Param('storeId') storeId: string,
    @Query() query: SaleExportQueryDto,
    @Res() res: Response,
  ): Promise<void> {
    await this.saleService.export(Number(storeId), query);

    res.setHeader(
      'Content-Disposition',
      `attachment; filename=${this.saleService.FILE_PATH.split('/')[2]}`,
    );

    const readStream = createReadStream(this.saleService.FILE_PATH);
    readStream.pipe(res);
  }

  @Get('index/mine')
  @HttpCode(HttpStatus.OK)
  async indexMine(
    @Query() query: SaleIndexMineQueryDto,
  ): Promise<SaleIndexPaginatedDto> {
    return await this.saleService.indexMine(query);
  }

  @Get('index/order/by-token')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  async indexOrderByToken(
    @Req() request: any,
    @Query() query: SaleIndexOrderByTokenQueryDto,
  ): Promise<SaleIndexPaginatedDto> {
    const accountId = request['account'].id;

    return await this.saleService.indexOrderByToken(Number(accountId), query);
  }

  @Post('/create')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.CREATED)
  async store(
    @Req() request: any,
    @Body() body: SaleStoreFormDto,
  ): Promise<SalePixCopyPaste | void> {
    const accountId = request['account'].id;

    return await this.saleService.store(Number(accountId), body);
  }

  @Get(':id/resend-email')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  async resendEmail(@Param('id') id: string): Promise<void> {
    await this.saleService.resendEmail(Number(id));
  }

  @Patch(':id/edit')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  async edit(@Param('id') id: string, @Body() body: SaleUpdateFormDto) {
    await this.saleService.edit(Number(id), body);
  }

  @Patch(':id/status')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.OK)
  async statusUpdate(
    @Param('id') id: string,
    @Body() body: SaleStatusUpdateDto,
  ): Promise<void> {
    await this.saleService.statusUpdate(Number(id), body);
  }
}
