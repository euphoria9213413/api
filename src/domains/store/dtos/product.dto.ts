import { SaleDataDto } from 'src/domains/sale/dtos/sale.dto';
import { StoreDataDto } from './store.dto';

export type ProductDataDto = {
  id: number;
  name: string;
  unitaryValue: string;
  graduateMargin: string;
  goalUnit: number;
  goalAmount: string;
  store?: StoreDataDto;
  sales?: SaleDataDto[];
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
};
