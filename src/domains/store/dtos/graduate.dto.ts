import { StoreDataDto } from './store.dto';

export type GraduateDataDto = {
  id: number;
  code: string;
  name: string;
  cpf: string;
  class: string;
  situation: boolean;
  store: StoreDataDto;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
};

export type GraduateIndexDto = {
  id: number;
  code: string;
  name: string;
  cpf: string;
  class: string;
  situation: boolean;
  store: StoreDataDto;
};

export type GraduateIndexPaginatedDto = {
  data: GraduateIndexDto[];
  page: number;
  perPage: number;
  lastPage: number;
  total: number;
};

export type GraduateByCodeDto = Omit<GraduateIndexDto, 'store'>;

export type GraduateImportedDto = {
  store: Pick<StoreDataDto, 'id'>;
} & Omit<
  GraduateDataDto,
  'id' | 'situation' | 'store' | 'createdAt' | 'updatedAt' | 'deletedAt'
>;
