import { FeePolicyType } from '../enums/fee-policy.enum';
import { MediaDataDto } from 'src/domains/storage/dtos/media.dto';
import { ProductDataDto } from './product.dto';

export type StoreDataDto = {
  id: number;
  name: string;
  slug: string;
  initialDate: Date;
  finalDate: Date;
  raffleDate: Date;
  situation: boolean;
  raffleUrl: string;
  feePolicy: FeePolicyType;
  nickname: string;
  videoUrl: string;
  regulationUrl: string;
  description: string;
  desktopMedia: MediaDataDto;
  mobileMedia: MediaDataDto;
  products: ProductDataDto[];
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
};

type Goal = {
  goalAmount: number;
  goalAchieved: number;
  goalPercent: string;
};

export type StoreIndexQueryDto = Omit<StoreIndexDto, 'goal'> & {
  total_goal_amount: number;
  total_goal_achieved: number;
  total: number;
};

export type StoreIndexDto = {
  id: number;
  name: string;
  slug: string;
  initialDate: Date;
  finalDate: Date;
  raffleDate: Date;
  situation: boolean;
  goal: Goal;
};

export type StoreIndexPaginatedDto = {
  data: StoreIndexDto[];
  page: number;
  perPage: number;
  lastPage: number;
  total: number;
};

export type StoreNameDto = Pick<StoreDataDto, 'name'>;

export type StoreEventDto = {
  id: number;
  name: string;
  slug: string;
  raffleDate: Date;
  initialDate: Date;
  finalDate: Date;
  videoUrl?: string;
  raffleUrl?: string;
  feePolicy?: FeePolicyType;
  regulationUrl?: string;
  description?: string;
  products: Pick<ProductDataDto, 'id' | 'name' | 'unitaryValue'>[];
  desktopMediaUrl?: string;
  mobileMediaUrl?: string;
};
