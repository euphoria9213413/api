import { Module } from '@nestjs/common';
import { StoreController } from './contollers/store.controller';
import { StoreService } from './services/store.service';
import { StoreRepository } from './repositories/store.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StoreEntity } from './entities/store.entity';
import { StorageModule } from '../storage/storage.module';
import { MediaModule } from '../storage/media.module';
import { AccountModule } from '../account/account.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([StoreEntity]),
    AccountModule,
    StorageModule,
    MediaModule,
  ],
  controllers: [StoreController],
  providers: [StoreService, StoreRepository],
})
export class StoreModule {}
