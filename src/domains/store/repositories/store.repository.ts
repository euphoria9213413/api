import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { StoreEntity } from '../entities/store.entity';
import {
  StoreDataDto,
  StoreEventDto,
  StoreIndexPaginatedDto,
} from '../dtos/store.dto';
import { storeIndexMapper } from '../mappers/store-index.mapper';
import {
  StoreCreateDto,
  StoreEditDto,
  StoreEventIndexQueryDto,
  StoreIndexQueryDto,
} from '../form-validations/store.form-validation';
import { storeEventIndexMapper } from '../mappers/store-event-index.mapper';
import { storeEventBySlugMapper } from '../mappers/store-event-by-slug.mapper';
import { StoreBySlugNotFoundException } from '../exceptions/store-by-slug-not-found.exception';

@Injectable()
export class StoreRepository extends Repository<StoreEntity> {
  constructor(private dataSource: DataSource) {
    super(StoreEntity, dataSource.createEntityManager());
  }

  async getAllByQuery(filter: StoreIndexQueryDto): Promise<any> {
    const page = Number(filter.page);
    const perPage = Number(filter.perPage);

    let situationFilter = [true, false];
    switch (filter.situation) {
      case 'ativos':
        situationFilter = [true];
        break;
      case 'inativos':
        situationFilter = [false];
        break;
      default:
        break;
    }

    const nameFilter = filter.name ? `%${filter.name}%` : '%%';

    const query = `
      select 
        s.id, 
        s.name,
        s.slug, 
        s."initialDate", 
        s."finalDate", 
        s."raffleDate", 
        s.situation, 
        sum(p."goalAmount") as total_goal_amount, 
        store_with_achieved.total_goal_achieved,
        sum(1) over () as total
      from store s 
      join product p on p."storeId" = s.id
      join (
        select 
          distinct s.id,
          sum(case when p2.status is null then 0 else p."unitaryValue" end) over (partition by s.id) as total_goal_achieved
        from store s 
        join product p on p."storeId" = s.id
        left join sale s2 on s2."productId" = p.id
        left join payment p2 on p2.id = s2."paymentId" 
        where p2.status = 'concluida' or p2.status is null
      ) as store_with_achieved on store_with_achieved.id = s.id
      where s."name" ilike $1 and s.situation = ANY($2::boolean[])
      group by s.id, s.name, s.slug, s."initialDate", s."finalDate", s."raffleDate", s.situation, store_with_achieved.total_goal_achieved
      order by s."initialDate" 
      limit $3 offset $4
    `;

    const result = await this.query(query, [
      nameFilter,
      situationFilter,
      perPage,
      (page - 1) * perPage,
    ]);

    return storeIndexMapper(result, page, perPage);
  }

  async getNameById(id: number): Promise<string> {
    return (await this.findOneByOrFail({ id })).name;
  }

  async findById(id: number): Promise<StoreDataDto> {
    return await this.findOneOrFail({
      where: { id },
      relations: ['products', 'desktopMedia', 'mobileMedia'],
    });
  }

  async updateById(id: number, body: any): Promise<void> {
    await this.update({ id }, { ...body });
  }

  async existsByName(name: string): Promise<boolean> {
    return await this.exist({ where: { name } });
  }

  async existsBySlug(slug: string): Promise<boolean> {
    return await this.exist({ where: { slug } });
  }

  async store(data: StoreCreateDto): Promise<StoreDataDto> {
    return await this.save(data);
  }

  async editById(id: number, data: StoreEditDto): Promise<void> {
    await this.save({
      id,
      ...data,
    });
  }

  async getAllEvents(query: StoreEventIndexQueryDto): Promise<StoreEventDto[]> {
    const result = await this.createQueryBuilder('store')
      .innerJoin('store.products', 'products')
      .innerJoin('store.desktopMedia', 'desktopMedia')
      .innerJoin('store.mobileMedia', 'mobileMedia')
      .select('store')
      .addSelect('products')
      .addSelect('desktopMedia')
      .addSelect('mobileMedia')
      .where('store.situation = true')
      .andWhere("store.initialDate <= now() - INTERVAL '3 hours'")
      .andWhere("store.finalDate >= now() - INTERVAL '3 hours'")
      .andWhere('store.name ilike :name', {
        name: `%${query.name || ''}%`,
      })
      .orderBy('store.raffleDate', 'ASC')
      .getMany();

    return storeEventIndexMapper(result);
  }

  async getEventBySlug(slug: string): Promise<StoreEventDto> {
    const result = await this.createQueryBuilder('store')
      .innerJoin('store.products', 'products')
      .innerJoin('store.desktopMedia', 'desktopMedia')
      .innerJoin('store.mobileMedia', 'mobileMedia')
      .select('store')
      .addSelect('products')
      .addSelect('desktopMedia')
      .addSelect('mobileMedia')
      .where('store.situation = true')
      .andWhere("store.initialDate <= now() - INTERVAL '3 hours'")
      .andWhere("store.finalDate >= now() - INTERVAL '3 hours'")
      .andWhere('store.slug = :slug', { slug })
      .getOne();

    if (!result) throw new StoreBySlugNotFoundException(slug);

    return storeEventBySlugMapper(result);
  }
}
