import { Injectable } from '@nestjs/common';
import { Brackets, DataSource, ILike, Repository } from 'typeorm';
import { GraduateEntity } from '../entities/graduate.entity';
import {
  GraduateByCodeDto,
  GraduateDataDto,
  GraduateImportedDto,
  GraduateIndexPaginatedDto,
} from '../dtos/graduate.dto';
import {
  GraduateIndexQueryDto,
  GraduateStoreDto,
} from '../form-validations/graduate.form-validation';
import { graduateIndexMapper } from '../mappers/graduate-index.mapper';
import { graduateByCodeMapper } from '../mappers/graduate-by-code.mapper';

@Injectable()
export class GraduateRepository extends Repository<GraduateEntity> {
  constructor(private readonly dataSource: DataSource) {
    super(GraduateEntity, dataSource.createEntityManager());
  }

  async getAllByQuery(
    storeId: number,
    query: GraduateIndexQueryDto,
  ): Promise<GraduateIndexPaginatedDto> {
    const page = Number(query.page);
    const perPage = Number(query.perPage);

    let filter = new Brackets((db) => {
      db.where('graduate.code ilike :code', {
        code: `%${query.codeOrNameOrClass || ''}%`,
      })
        .orWhere('graduate.name ilike :name', {
          name: `%${query.codeOrNameOrClass || ''}%`,
        })
        .orWhere('graduate.class ilike :class', {
          class: `%${query.codeOrNameOrClass || ''}%`,
        });
    });

    if (query.name) {
      filter = new Brackets((db) => {
        db.where('graduate.name ilike :name', {
          name: `%${query.name || ''}%`,
        });
      });
    }

    const queryBuilder = this.createQueryBuilder('graduate')
      .innerJoin('graduate.store', 'store')
      .select('graduate')
      .addSelect('store')
      .where('store.id = :storeId', { storeId })
      .andWhere(filter);

    switch (query.situation) {
      case 'ativos':
        queryBuilder.andWhere('graduate.situation = :situations', {
          situations: true,
        });
        break;
      case 'inativos':
        queryBuilder.andWhere('graduate.situation = :situations', {
          situations: false,
        });
      default:
        break;
    }

    const [result, total] = await queryBuilder
      .orderBy('graduate.code', 'ASC')
      .skip((page - 1) * perPage)
      .take(perPage)
      .getManyAndCount();

    return graduateIndexMapper(result, page, perPage, total);
  }

  async findById(id: number): Promise<GraduateDataDto> {
    return await this.findOneByOrFail({ id });
  }

  async store(data: GraduateStoreDto): Promise<GraduateDataDto> {
    return await this.save(data);
  }

  async updateById(id: number, body: any): Promise<void> {
    await this.update({ id }, { ...body });
  }

  async existsByUniqueCodeCpfStoreIdConstraint(
    code: string,
    cpf: string,
    storeId: number,
  ): Promise<boolean> {
    return await this.createQueryBuilder('graduate')
      .innerJoin('graduate.store', 'store')
      .where('graduate.code = :code', { code })
      .andWhere('graduate.cpf = :cpf', { cpf })
      .andWhere('store.id = :storeId', { storeId })
      .getExists();
  }

  async createIfNotExists(
    data: GraduateImportedDto[],
    conflictPaths: string[],
  ): Promise<void> {
    await this.upsert(data, {
      conflictPaths,
      skipUpdateIfNoValuesChanged: true,
    });
  }

  async getByCode(storeId: number, code: string): Promise<GraduateByCodeDto> {
    const result = await this.createQueryBuilder('graduate')
      .innerJoin('graduate.store', 'store')
      .select('graduate')
      .where('store.id = :storeId', { storeId })
      .andWhere('graduate.code = :code', { code })
      .getOne();

    return graduateByCodeMapper(result);
  }
}
