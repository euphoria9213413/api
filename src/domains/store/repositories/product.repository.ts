import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { ProductEntity } from '../entities/product.entity';
import { ProductDataDto } from '../dtos/product.dto';
import { ProductStoreDto } from '../form-validations/product.form-validation';

@Injectable()
export class ProductRepository extends Repository<ProductEntity> {
  constructor(private readonly dataSource: DataSource) {
    super(ProductEntity, dataSource.createEntityManager());
  }

  async existsByUniqueNameStoreIdConstraint(
    name: string,
    storeId: number,
  ): Promise<boolean> {
    return await this.createQueryBuilder('product')
      .innerJoin('product.store', 'store')
      .where('product.name = :name', { name })
      .andWhere('store.id = :storeId', { storeId })
      .getExists();
  }

  async store(data: ProductStoreDto): Promise<ProductDataDto> {
    return await this.save(data);
  }

  async editById(id: number, data: ProductStoreDto): Promise<ProductDataDto> {
    return await this.save({
      id,
      ...data,
    });
  }

  async findById(id: number): Promise<ProductDataDto> {
    return await this.findOneByOrFail({ id });
  }

  async destroy(id: number): Promise<void> {
    await this.softDelete({ id });
  }
}
