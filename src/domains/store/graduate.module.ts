import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GraduateRepository } from './repositories/graduate.repository';
import { GraduateController } from './contollers/graduate.controller';
import { GraduateEntity } from './entities/graduate.entity';
import { GraduateService } from './services/graduate.service';
import { AccountModule } from '../account/account.module';

@Module({
  imports: [TypeOrmModule.forFeature([GraduateEntity]), AccountModule],
  controllers: [GraduateController],
  providers: [GraduateService, GraduateRepository],
})
export class GraduateModule {}
