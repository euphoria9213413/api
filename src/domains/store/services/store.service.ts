import { Injectable } from '@nestjs/common';
import { StoreRepository } from '../repositories/store.repository';
import {
  StoreDataDto,
  StoreEventDto,
  StoreIndexPaginatedDto,
  StoreNameDto,
} from '../dtos/store.dto';
import {
  StoreCreateDto,
  StoreEditDto,
  StoreEventIndexQueryDto,
  StoreImagesUploadDto,
  StoreIndexQueryDto,
  StoreSituationUpdateDto,
} from '../form-validations/store.form-validation';
import { StoreNotFoundException } from '../exceptions/store-not-found.exception';
import { StoreNameAlreadyExistsException } from '../exceptions/store-name-already-exists.exception';
import { StoreSlugAlreadyExistsException } from '../exceptions/store-slug-already-exists.exception';
import { StorageService } from 'src/domains/storage/services/storage.service';
import { MediaService } from 'src/domains/storage/services/media.service';
import { UploadFileException } from 'src/domains/storage/exceptions/upload-file.exception';

@Injectable()
export class StoreService {
  constructor(
    private readonly storeRepository: StoreRepository,
    private readonly storageService: StorageService,
    private readonly mediaService: MediaService,
  ) {}

  async index(query: StoreIndexQueryDto): Promise<StoreIndexPaginatedDto> {
    return await this.storeRepository.getAllByQuery(query);
  }

  async show(id: number): Promise<StoreDataDto> {
    try {
      return await this.storeRepository.findById(id);
    } catch {
      throw new StoreNotFoundException(id);
    }
  }

  async statusUpdate(id: number, body: StoreSituationUpdateDto): Promise<void> {
    try {
      await this.storeRepository.findById(id);
    } catch {
      throw new StoreNotFoundException(id);
    }

    await this.storeRepository.updateById(id, body);
  }

  async store(data: StoreCreateDto): Promise<StoreDataDto> {
    if (await this.storeRepository.existsByName(data.name))
      throw new StoreNameAlreadyExistsException();

    if (await this.storeRepository.existsBySlug(data.slug))
      throw new StoreSlugAlreadyExistsException();

    return await this.storeRepository.store(data);
  }

  async imageUpload(id: number, data: StoreImagesUploadDto): Promise<void> {
    try {
      await this.storeRepository.findById(id);
    } catch {
      throw new StoreNotFoundException(id);
    }

    try {
      if (data.desktopMedia) {
        const desktopMediaData = await this.storageService.upload(
          data.desktopMedia[0],
        );
        const desktopMediaStored = await this.mediaService.store(
          desktopMediaData,
        );

        await this.storeRepository.updateById(id, {
          desktopMedia: desktopMediaStored.id,
        });
      }

      if (data.mobileMedia) {
        const mobileMediaData = await this.storageService.upload(
          data.mobileMedia[0],
        );
        const mobileMediaStored = await this.mediaService.store(
          mobileMediaData,
        );

        await this.storeRepository.updateById(id, {
          mobileMedia: mobileMediaStored.id,
        });
      }
    } catch {
      throw new UploadFileException();
    }
  }

  async edit(id: number, data: StoreEditDto): Promise<void> {
    try {
      await this.storeRepository.findById(id);
    } catch {
      throw new StoreNotFoundException(id);
    }

    return await this.storeRepository.editById(id, data);
  }

  async getName(id: number): Promise<StoreNameDto> {
    const name = await this.storeRepository.getNameById(id);

    return {
      name,
    };
  }

  async eventIndex(query: StoreEventIndexQueryDto): Promise<StoreEventDto[]> {
    return await this.storeRepository.getAllEvents(query);
  }

  async eventBySlug(slug: string): Promise<StoreEventDto> {
    return await this.storeRepository.getEventBySlug(slug);
  }
}
