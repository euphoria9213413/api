import { Injectable } from '@nestjs/common';
import { ProductRepository } from '../repositories/product.repository';
import { ProductDataDto } from '../dtos/product.dto';
import { ProductNotFoundException } from '../exceptions/product-not-found.exception';
import { ProductStoreDto } from '../form-validations/product.form-validation';
import { ProductUniqueConstraintException } from '../exceptions/product-unique-constraint.exception';

@Injectable()
export class ProductService {
  constructor(private readonly productRepository: ProductRepository) {}

  async store(storeId: number, data: ProductStoreDto): Promise<ProductDataDto> {
    const graduateAlreadyExists =
      await this.productRepository.existsByUniqueNameStoreIdConstraint(
        data.name,
        storeId,
      );

    if (graduateAlreadyExists)
      throw new ProductUniqueConstraintException(storeId);

    data.store = {
      id: storeId,
    };

    return await this.productRepository.store(data);
  }

  async edit(id: number, data: ProductStoreDto): Promise<ProductDataDto> {
    try {
      await this.productRepository.findById(id);
    } catch {
      throw new ProductNotFoundException(id);
    }

    return await this.productRepository.editById(id, data);
  }

  async show(id: number): Promise<ProductDataDto> {
    try {
      return await this.productRepository.findById(id);
    } catch {
      throw new ProductNotFoundException(id);
    }
  }

  async destroy(id: number): Promise<void> {
    try {
      await this.productRepository.findById(id);
    } catch {
      throw new ProductNotFoundException(id);
    }

    await this.productRepository.destroy(id);
  }
}
