import { Injectable } from '@nestjs/common';
import { GraduateRepository } from '../repositories/graduate.repository';
import {
  GraduateByCodeDto,
  GraduateDataDto,
  GraduateIndexPaginatedDto,
} from '../dtos/graduate.dto';
import { GraduateNotFoundException } from '../exceptions/graduate-not-found.exception';
import {
  GraduateIndexQueryDto,
  GraduateSituationUpdateDto,
  GraduateStoreDto,
} from '../form-validations/graduate.form-validation';
import { GraduateUniqueConstraintException } from '../exceptions/graduate-unique-constraint.exception';
import { XlsxFileUtil } from 'src/utils/xlsx-file.util';
import { graduateImportedMapper } from '../mappers/graduate-imported.mapper';
import * as lodash from 'lodash';
import { GraduateImportException } from '../exceptions/graduate-import.exception';
import { GraduateByCodeNotFoundException } from '../exceptions/graduate-by-code-not-found.exception';

@Injectable()
export class GraduateService {
  constructor(private readonly graduateRepository: GraduateRepository) {}

  async index(
    storeId: number,
    query: GraduateIndexQueryDto,
  ): Promise<GraduateIndexPaginatedDto> {
    return await this.graduateRepository.getAllByQuery(storeId, query);
  }

  async store(
    storeId: number,
    data: GraduateStoreDto,
  ): Promise<GraduateDataDto> {
    const graduateAlreadyExists =
      await this.graduateRepository.existsByUniqueCodeCpfStoreIdConstraint(
        data.code,
        data.cpf,
        storeId,
      );

    if (graduateAlreadyExists)
      throw new GraduateUniqueConstraintException(storeId);

    data.store = {
      id: storeId,
    };

    return await this.graduateRepository.store(data);
  }

  async statusUpdate(
    id: number,
    body: GraduateSituationUpdateDto,
  ): Promise<void> {
    try {
      await this.graduateRepository.findById(id);
    } catch {
      throw new GraduateNotFoundException(id);
    }

    await this.graduateRepository.updateById(id, body);
  }

  async importFile(storeId: number, path: string): Promise<void> {
    const fileContent = XlsxFileUtil.readFile(path);
    const data = graduateImportedMapper(storeId, fileContent);
    const chunkData = lodash.chunk(data, 1000);

    try {
      Promise.all(
        chunkData.map(
          async (element) =>
            await this.graduateRepository.createIfNotExists(element, [
              'code',
              'cpf',
              'store',
            ]),
        ),
      );
    } catch {
      throw new GraduateImportException();
    }
  }

  async getByCode(storeId: number, code: string): Promise<GraduateByCodeDto> {
    try {
      return await this.graduateRepository.getByCode(storeId, code);
    } catch {
      throw new GraduateByCodeNotFoundException(code);
    }
  }
}
