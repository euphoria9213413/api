import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { StoreDataDto } from '../dtos/store.dto';

export class GraduateIndexQueryDto {
  @IsOptional()
  codeOrNameOrClass?: string;

  @IsOptional()
  name?: string;

  @IsOptional()
  situation?: string;

  @IsNotEmpty({ message: 'Página deve ser informada' })
  page: string;

  @IsNotEmpty({ message: 'Quantidade por página deve ser informada' })
  perPage: string;
}

export class GraduateStoreDto {
  @IsNotEmpty({ message: 'Código deve ser informado' })
  @IsString({ message: 'Código deve ser do tipo texto' })
  code: string;

  @IsNotEmpty({ message: 'Nome deve ser informado' })
  @IsString({ message: 'Nome deve ser do tipo texto' })
  name: string;

  @IsNotEmpty({ message: 'CPF deve ser informado' })
  cpf: string;

  @IsNotEmpty({ message: 'Turma deve ser informado' })
  @IsString({ message: 'Turma deve ser do tipo texto' })
  class: string;

  @IsNotEmpty({ message: 'Situação deve ser informado' })
  @IsBoolean({ message: 'Situação deve ser do tipo booleano' })
  situation: boolean;

  store: Pick<StoreDataDto, 'id'>;
}

export class GraduateSituationUpdateDto {
  @IsNotEmpty({ message: 'Situação deve ser informada' })
  @IsBoolean({ message: 'Situação deve ser do tipo booleano' })
  situation: boolean;
}
