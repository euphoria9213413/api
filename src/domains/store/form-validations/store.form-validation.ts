import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { FeePolicyType } from '../enums/fee-policy.enum';
import { ProductStoreDto } from './product.form-validation';

export class StoreIndexQueryDto {
  @IsOptional()
  name?: string;

  @IsOptional()
  situation?: string;

  @IsNotEmpty({ message: 'Página deve ser informada' })
  page: string;

  @IsNotEmpty({ message: 'Quantidade por página deve ser informada' })
  perPage: string;
}

export class StoreSituationUpdateDto {
  @IsNotEmpty({ message: 'Situação deve ser informada' })
  @IsBoolean({ message: 'Situação deve ser do tipo booleano' })
  situation: boolean;
}

export class StoreCreateDto {
  @IsNotEmpty({ message: 'Nome deve ser informado' })
  @IsString({ message: 'Nome deve ser do tipo texto' })
  name: string;

  @IsNotEmpty({ message: 'Slug deve ser informado' })
  @IsString({ message: 'Slug deve ser do tipo texto' })
  slug: string;

  @IsNotEmpty({ message: 'Data de início das vendas deve ser informada' })
  initialDate: Date;

  @IsNotEmpty({ message: 'Data de fim das vendas deve ser informada' })
  finalDate: Date;

  @IsOptional()
  raffleDate: Date;

  @IsBoolean({ message: 'Situação deve ser do tipo booleana' })
  situation: boolean;

  @IsOptional()
  @IsString({ message: 'Link do sorteio deve ser do tipo texto' })
  raffleUrl: string;

  @IsNotEmpty({ message: 'Política de taxas deve ser informada' })
  @IsString({ message: 'Política de taxas deve ser do tipo texto' })
  feePolicy: FeePolicyType;

  @IsOptional()
  @IsString({ message: 'Apelido da loja deve ser do tipo texto' })
  nickname: string;

  @IsOptional()
  @IsString({ message: 'Url do vídeo deve ser do tipo texto' })
  videoUrl: string;

  @IsOptional()
  @IsString({ message: 'Link do regulamento deve ser do tipo texto' })
  regulationUrl: string;

  @IsOptional()
  @IsString({ message: 'Descrição deve ser do tipo texto' })
  description: string;

  @IsOptional()
  products: ProductStoreDto[];
}

export class StoreEditDto {
  @IsNotEmpty({ message: 'Nome deve ser informado' })
  @IsString({ message: 'Nome deve ser do tipo texto' })
  name: string;

  @IsNotEmpty({ message: 'Slug deve ser informado' })
  @IsString({ message: 'Slug deve ser do tipo texto' })
  slug;

  @IsNotEmpty({ message: 'Data de início das vendas deve ser informada' })
  initialDate: Date;

  @IsNotEmpty({ message: 'Data de fim das vendas deve ser informada' })
  finalDate: Date;

  @IsOptional()
  raffleDate: Date;

  @IsBoolean({ message: 'Situação deve ser do tipo booleana' })
  situation: boolean;

  @IsOptional()
  @IsString({ message: 'Link do sorteio deve ser do tipo texto' })
  raffleUrl: string;

  @IsNotEmpty({ message: 'Política de taxas deve ser informada' })
  @IsString({ message: 'Política de taxas deve ser do tipo texto' })
  feePolicy: FeePolicyType;

  @IsOptional()
  @IsString({ message: 'Apelido da loja deve ser do tipo texto' })
  nickname: string;

  @IsOptional()
  @IsString({ message: 'Url do vídeo deve ser do tipo texto' })
  videoUrl: string;

  @IsOptional()
  @IsString({ message: 'Link do regulamento deve ser do tipo texto' })
  regulationUrl: string;

  @IsOptional()
  @IsString({ message: 'Descrição deve ser do tipo texto' })
  description: string;
}

export class StoreImagesUploadDto {
  @IsOptional()
  desktopMedia: Express.Multer.File;

  @IsOptional()
  mobileMedia: Express.Multer.File;
}

export class StoreEventIndexQueryDto {
  @IsOptional()
  name?: string;
}
