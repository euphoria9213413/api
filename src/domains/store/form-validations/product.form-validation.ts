import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { StoreDataDto } from '../dtos/store.dto';

export class ProductStoreDto {
  @IsNotEmpty({ message: 'Nome deve ser informado' })
  @IsString({ message: 'Nome deve ser do tipo texto' })
  name: string;

  @IsNotEmpty({ message: 'Valor unitário deve ser informado' })
  unitaryValue: string;

  @IsOptional()
  graduateMargin: string;

  @IsOptional()
  goalUnit: number;

  @IsNotEmpty({ message: 'Meta total deve ser informada' })
  goalAmount: string;

  store: Pick<StoreDataDto, 'id'>;
}
