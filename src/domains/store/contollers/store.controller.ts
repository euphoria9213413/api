import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  Query,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { StoreService } from '../services/store.service';
import {
  StoreDataDto,
  StoreEventDto,
  StoreIndexPaginatedDto,
  StoreNameDto,
} from '../dtos/store.dto';
import { AuthGuard } from 'src/config/guards/auth.guard';
import { RoleAdminGuard } from 'src/config/guards/role-admin.guard';
import {
  StoreCreateDto,
  StoreEditDto,
  StoreEventIndexQueryDto,
  StoreImagesUploadDto,
  StoreIndexQueryDto,
  StoreSituationUpdateDto,
} from '../form-validations/store.form-validation';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { imageFileFilter } from 'src/utils/file-validation.util';

@Controller('store')
export class StoreController {
  constructor(private readonly storeService: StoreService) {}

  @Get('/index')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.OK)
  async index(
    @Query() query: StoreIndexQueryDto,
  ): Promise<StoreIndexPaginatedDto> {
    return await this.storeService.index(query);
  }
  @Get('/events')
  @HttpCode(HttpStatus.OK)
  async eventIndex(
    @Query() query: StoreEventIndexQueryDto,
  ): Promise<StoreEventDto[]> {
    return await this.storeService.eventIndex(query);
  }

  @Get('event/:slug')
  @HttpCode(HttpStatus.OK)
  async eventBySlug(@Param('slug') slug: string): Promise<StoreEventDto> {
    return await this.storeService.eventBySlug(slug);
  }

  @Get(':id')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.OK)
  async show(@Param('id') id: string): Promise<StoreDataDto> {
    return await this.storeService.show(Number(id));
  }

  @Get(':id/name')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.OK)
  async getName(@Param('id') id: string): Promise<StoreNameDto> {
    return await this.storeService.getName(Number(id));
  }

  @Patch(':id/situation-update')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  async statusUpdate(
    @Param('id') id: string,
    @Body() body: StoreSituationUpdateDto,
  ): Promise<void> {
    await this.storeService.statusUpdate(Number(id), body);
  }

  @Post('create')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.CREATED)
  async store(@Body() body: StoreCreateDto): Promise<StoreDataDto> {
    return await this.storeService.store(body);
  }

  @Put(':id')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.OK)
  async edit(
    @Param('id') id: string,
    @Body() body: StoreEditDto,
  ): Promise<void> {
    await this.storeService.edit(Number(id), body);
  }

  @Post(':id/medias')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @UseInterceptors(
    FileFieldsInterceptor(
      [
        { name: 'desktopMedia', maxCount: 1 },
        { name: 'mobileMedia', maxCount: 1 },
      ],
      {
        fileFilter: (req, file, callback) =>
          imageFileFilter(req, file, callback, ['.jpg', '.jpeg', '.png']),
      },
    ),
  )
  @HttpCode(HttpStatus.OK)
  async imagesUpload(
    @Param('id') id: string,
    @UploadedFiles() files: StoreImagesUploadDto,
  ): Promise<void> {
    await this.storeService.imageUpload(Number(id), files);
  }
}
