import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from 'src/config/guards/auth.guard';
import { RoleAdminGuard } from 'src/config/guards/role-admin.guard';
import { GraduateService } from '../services/graduate.service';
import {
  GraduateIndexQueryDto,
  GraduateSituationUpdateDto,
  GraduateStoreDto,
} from '../form-validations/graduate.form-validation';
import {
  GraduateByCodeDto,
  GraduateDataDto,
  GraduateIndexPaginatedDto,
} from '../dtos/graduate.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express } from 'express';
import { unlink } from 'fs';
import { diskStorage } from 'multer';
import { imageFileFilter } from 'src/utils/file-validation.util';

@Controller('graduate')
export class GraduateController {
  constructor(private readonly graduateService: GraduateService) {}

  @Get(':storeId/index')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  async index(
    @Param('storeId') storeId: string,
    @Query() query: GraduateIndexQueryDto,
  ): Promise<GraduateIndexPaginatedDto> {
    return await this.graduateService.index(Number(storeId), query);
  }

  @Get(':storeId/:code')
  @UseGuards(AuthGuard)
  @HttpCode(HttpStatus.OK)
  async getByCode(
    @Param('storeId') storeId: string,
    @Param('code') code: string,
  ): Promise<GraduateByCodeDto> {
    return await this.graduateService.getByCode(Number(storeId), code);
  }

  @Post(':storeId/create')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.CREATED)
  async store(
    @Param('storeId') storeId: string,
    @Body() body: GraduateStoreDto,
  ): Promise<GraduateDataDto> {
    return await this.graduateService.store(Number(storeId), body);
  }

  @Patch(':id/situation-update')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  async statusUpdate(
    @Param('id') id: string,
    @Body() body: GraduateSituationUpdateDto,
  ): Promise<void> {
    await this.graduateService.statusUpdate(Number(id), body);
  }

  @Post(':storeId/file')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './files',
      }),
      fileFilter: (req, file, callback) =>
        imageFileFilter(req, file, callback, ['.xlsx', '.csv']),
    }),
  )
  @HttpCode(HttpStatus.NO_CONTENT)
  async uploadFile(
    @Param('storeId') storeId: string,
    @UploadedFile()
    file: Express.Multer.File,
  ): Promise<void> {
    await this.graduateService.importFile(Number(storeId), file.path);
    unlink(file.path, () => null);
  }
}
