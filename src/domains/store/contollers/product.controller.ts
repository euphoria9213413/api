import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { ProductService } from '../services/product.service';
import { AuthGuard } from 'src/config/guards/auth.guard';
import { RoleAdminGuard } from 'src/config/guards/role-admin.guard';
import { ProductDataDto } from '../dtos/product.dto';
import { ProductStoreDto } from '../form-validations/product.form-validation';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get(':id')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.OK)
  async show(@Param('id') id: string): Promise<ProductDataDto> {
    return await this.productService.show(Number(id));
  }

  @Post(':storeId/create')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.CREATED)
  async store(
    @Param('storeId') storeId: string,
    @Body() body: ProductStoreDto,
  ): Promise<ProductDataDto> {
    return await this.productService.store(Number(storeId), body);
  }

  @Put(':id')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.OK)
  async edit(
    @Param('id') id: string,
    @Body() body: ProductStoreDto,
  ): Promise<ProductDataDto> {
    return await this.productService.edit(Number(id), body);
  }

  @Delete(':id')
  @UseGuards(AuthGuard, RoleAdminGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  async destroy(@Param('id') id: string): Promise<void> {
    await this.productService.destroy(Number(id));
  }
}
