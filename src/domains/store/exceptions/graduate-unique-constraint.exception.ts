import { HttpException, HttpStatus } from '@nestjs/common';

export class GraduateUniqueConstraintException extends HttpException {
  constructor(sotreId: number) {
    super(
      `Formando com o mesmo código e cpf já cadastrado para a loja de id ${sotreId}`,
      HttpStatus.NOT_FOUND,
    );
  }
}
