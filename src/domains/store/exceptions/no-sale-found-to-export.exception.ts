import { HttpException, HttpStatus } from '@nestjs/common';

export class NoSaleFoundToExportException extends HttpException {
  constructor() {
    super(
      `Nenhuma venda à ser exportada com os filtros informados`,
      HttpStatus.BAD_REQUEST,
    );
  }
}
