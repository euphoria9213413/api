import { HttpException, HttpStatus } from '@nestjs/common';

export class GraduateNotFoundException extends HttpException {
  constructor(id: number) {
    super(`Formando com o id ${id} não encontrado`, HttpStatus.NOT_FOUND);
  }
}
