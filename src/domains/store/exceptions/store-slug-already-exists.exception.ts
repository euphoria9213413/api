import { HttpException, HttpStatus } from '@nestjs/common';

export class StoreSlugAlreadyExistsException extends HttpException {
  constructor() {
    super('Já existe uma loja com este slug', HttpStatus.BAD_REQUEST);
  }
}
