import { HttpException, HttpStatus } from '@nestjs/common';

export class GraduateImportException extends HttpException {
  constructor() {
    super(`Falha ao importar formandos`, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
