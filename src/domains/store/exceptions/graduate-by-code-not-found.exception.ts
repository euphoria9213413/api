import { HttpException, HttpStatus } from '@nestjs/common';

export class GraduateByCodeNotFoundException extends HttpException {
  constructor(code: string) {
    super(`Formando com o código ${code} não encontrado`, HttpStatus.NOT_FOUND);
  }
}
