import { HttpException, HttpStatus } from '@nestjs/common';

export class StoreNotFoundException extends HttpException {
  constructor(id: number) {
    super(`Loja com o id ${id} não encontrada`, HttpStatus.NOT_FOUND);
  }
}
