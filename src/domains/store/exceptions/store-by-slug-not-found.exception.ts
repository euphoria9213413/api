import { HttpException, HttpStatus } from '@nestjs/common';

export class StoreBySlugNotFoundException extends HttpException {
  constructor(slug: string) {
    super(`Evento com o slug ${slug} não encontrado`, HttpStatus.NOT_FOUND);
  }
}
