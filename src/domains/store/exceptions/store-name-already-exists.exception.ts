import { HttpException, HttpStatus } from '@nestjs/common';

export class StoreNameAlreadyExistsException extends HttpException {
  constructor() {
    super('Já existe uma loja com este nome', HttpStatus.BAD_REQUEST);
  }
}
