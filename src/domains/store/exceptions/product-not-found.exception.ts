import { HttpException, HttpStatus } from '@nestjs/common';

export class ProductNotFoundException extends HttpException {
  constructor(id: number) {
    super(`Produto com o id ${id} não encontrado`, HttpStatus.NOT_FOUND);
  }
}
