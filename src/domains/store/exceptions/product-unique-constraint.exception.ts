import { HttpException, HttpStatus } from '@nestjs/common';

export class ProductUniqueConstraintException extends HttpException {
  constructor(sotreId: number) {
    super(
      `Produto com o mesmo nome já cadastrado para a loja de id ${sotreId}`,
      HttpStatus.NOT_FOUND,
    );
  }
}
