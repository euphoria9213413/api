import { PtBrDataFormatterUtil } from 'src/utils/pt-br-data-formater.util';
import {
  StoreIndexDto,
  StoreIndexPaginatedDto,
  StoreIndexQueryDto,
} from '../dtos/store.dto';

export const storeIndexMapper = (
  data: StoreIndexQueryDto[],
  page: number,
  perPage: number,
): StoreIndexPaginatedDto => {
  const stores = data.map((store): StoreIndexDto => {
    const goalPercent =
      (store.total_goal_achieved * 100) / store.total_goal_amount || 0;

    return {
      id: store.id,
      name: store.name,
      slug: store.slug,
      initialDate: store.initialDate,
      finalDate: store.finalDate,
      raffleDate: store.raffleDate,
      situation: store.situation,
      goal: {
        goalAmount: Math.round(store.total_goal_amount),
        goalAchieved: Math.round(store.total_goal_achieved),
        goalPercent: goalPercent
          ? `${PtBrDataFormatterUtil.roundFloat(goalPercent)}%`
          : '0%',
      },
    };
  });

  return {
    data: stores,
    page,
    perPage,
    lastPage: Math.ceil(data[0]?.total / perPage),
    total: data[0]?.total,
  };
};
