import { GraduateByCodeDto, GraduateDataDto } from '../dtos/graduate.dto';

export const graduateByCodeMapper = (
  data: GraduateDataDto,
): GraduateByCodeDto => ({
  id: data.id,
  code: data.code,
  name: data.name,
  cpf: data.cpf,
  class: data.class,
  situation: data.situation,
});
