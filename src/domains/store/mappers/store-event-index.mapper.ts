import { StoreDataDto, StoreEventDto } from '../dtos/store.dto';

export const storeEventIndexMapper = (
  data: StoreDataDto[],
): StoreEventDto[] => {
  return data.map((store): StoreEventDto => {
    return {
      id: store.id,
      name: store.name,
      slug: store.slug,
      raffleDate: store.raffleDate,
      initialDate: store.initialDate,
      finalDate: store.finalDate,
      products: store.products.map((product) => {
        return {
          id: product.id,
          name: product.name,
          unitaryValue: product.unitaryValue,
        };
      }),
      desktopMediaUrl: store.desktopMedia.url,
      mobileMediaUrl: store.mobileMedia.url,
    };
  });
};
