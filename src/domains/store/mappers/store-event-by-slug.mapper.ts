import { StoreDataDto, StoreEventDto } from '../dtos/store.dto';

export const storeEventBySlugMapper = (store: StoreDataDto): StoreEventDto => {
  return {
    id: store.id,
    name: store.name,
    slug: store.slug,
    raffleDate: store.raffleDate,
    initialDate: store.initialDate,
    finalDate: store.finalDate,
    products: store.products.map((product) => {
      return {
        id: product.id,
        name: product.name,
        unitaryValue: product.unitaryValue,
      };
    }),
    videoUrl: store.videoUrl,
    raffleUrl: store.raffleUrl,
    feePolicy: store.feePolicy,
    regulationUrl: store.regulationUrl,
    description: store.description,
    desktopMediaUrl: store.desktopMedia.url,
    mobileMediaUrl: store.mobileMedia.url,
  };
};
