import {
  GraduateDataDto,
  GraduateIndexDto,
  GraduateIndexPaginatedDto,
} from '../dtos/graduate.dto';

export const graduateIndexMapper = (
  data: GraduateDataDto[],
  page: number,
  perPage: number,
  total: number,
): GraduateIndexPaginatedDto => {
  const graduates = data.map(
    (graduate): GraduateIndexDto => ({
      id: graduate.id,
      code: graduate.code,
      name: graduate.name,
      cpf: graduate.cpf,
      class: graduate.class,
      situation: graduate.situation,
      store: graduate.store,
    }),
  );

  return {
    data: graduates,
    page,
    perPage,
    lastPage: Math.ceil(total / perPage),
    total,
  };
};
