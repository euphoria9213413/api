import { GraduateImportedDto } from '../dtos/graduate.dto';

export const graduateImportedMapper = (
  storeId: number,
  data: any[],
): GraduateImportedDto[] =>
  data.map((value): GraduateImportedDto => {
    return {
      code: value[0],
      name: value[1],
      cpf: String(value[2]).replace(/[^0-9]/g, ''),
      class: value[3],
      store: { id: storeId },
    };
  });
