import { StoreEntity } from 'src/domains/store/entities/store.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { SaleEntity } from '../../sale/entities/sale.entity';

@Entity({
  schema: 'public',
  name: 'graduate',
  synchronize: false,
})
export class GraduateEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment', {
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'varchar',
    name: 'code',
  })
  code: string;

  @Column({
    type: 'varchar',
    name: 'name',
  })
  name: string;

  @Column({
    type: 'varchar',
    name: 'cpf',
  })
  cpf: string;

  @Column({
    type: 'varchar',
    name: 'class',
  })
  class: string;

  @Column({
    type: 'boolean',
    name: 'situation',
  })
  situation: boolean;

  @ManyToOne(() => StoreEntity, (store) => store.graduates, {
    persistence: false,
    cascade: false,
  })
  @Column({
    type: 'integer',
    name: 'storeId',
  })
  store: StoreEntity;

  @OneToMany(() => SaleEntity, (sale) => sale.graduate, {
    persistence: false,
    cascade: false,
  })
  sales: SaleEntity[];

  @CreateDateColumn({
    type: 'timestamp',
    name: 'createdAt',
    default: () => 'NOW()',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    name: 'updatedAt',
    default: () => 'NOW()',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    name: 'deletedAt',
  })
  deletedAt: Date;
}
