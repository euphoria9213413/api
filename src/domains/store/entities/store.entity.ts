import { MediaEntity } from 'src/domains/storage/entities/media.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ProductEntity } from './product.entity';
import { GraduateEntity } from 'src/domains/store/entities/graduate.entity';
import { FeePolicyType } from '../enums/fee-policy.enum';

@Entity({
  schema: 'public',
  name: 'store',
  synchronize: false,
})
export class StoreEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment', {
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'varchar',
    name: 'name',
  })
  name: string;

  @Column({
    type: 'varchar',
    name: 'slug',
  })
  slug: string;

  @Column({
    type: 'timestamp',
    name: 'initialDate',
  })
  initialDate: Date;

  @Column({
    type: 'timestamp',
    name: 'finalDate',
  })
  finalDate: Date;

  @Column({
    type: 'timestamp',
    name: 'raffleDate',
  })
  raffleDate: Date;

  @Column({
    type: 'boolean',
    name: 'situation',
  })
  situation: boolean;

  @Column({
    type: 'varchar',
    name: 'raffleUrl',
  })
  raffleUrl: string;

  @Column({
    type: 'varchar',
    name: 'feePolicy',
  })
  feePolicy: FeePolicyType;

  @Column({
    type: 'varchar',
    name: 'nickname',
  })
  nickname: string;

  @Column({
    type: 'varchar',
    name: 'videoUrl',
  })
  videoUrl: string;

  @Column({
    type: 'varchar',
    name: 'regulationUrl',
  })
  regulationUrl: string;

  @Column({
    type: 'text',
    name: 'description',
  })
  description: string;

  @OneToOne(() => MediaEntity, (media) => media.desktopStore, {
    persistence: false,
    cascade: false,
  })
  @JoinColumn()
  @Column({
    type: 'integer',
    name: 'desktopMediaId',
  })
  desktopMedia: MediaEntity;

  @OneToOne(() => MediaEntity, (media) => media.mobileStore, {
    persistence: false,
    cascade: false,
  })
  @JoinColumn()
  @Column({
    type: 'integer',
    name: 'mobileMediaId',
  })
  mobileMedia: MediaEntity;

  @OneToMany(() => ProductEntity, (product) => product.store, {
    persistence: true,
    cascade: true,
  })
  products: ProductEntity[];

  @OneToMany(() => GraduateEntity, (graduate) => graduate.store, {
    persistence: false,
    cascade: false,
  })
  graduates: GraduateEntity[];

  @CreateDateColumn({
    type: 'timestamp',
    name: 'createdAt',
    default: () => 'NOW()',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    name: 'updatedAt',
    default: () => 'NOW()',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    name: 'deletedAt',
  })
  deletedAt: Date;
}
