import {
  BaseEntity,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { StoreEntity } from './store.entity';
import { SaleEntity } from 'src/domains/sale/entities/sale.entity';

@Entity({
  schema: 'public',
  name: 'product',
  synchronize: false,
})
export class ProductEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment', {
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'varchar',
    name: 'name',
  })
  name: string;

  @Column({
    type: 'numeric',
    name: 'unitaryValue',
  })
  unitaryValue: string;

  @Column({
    type: 'numeric',
    name: 'graduateMargin',
  })
  graduateMargin: string;

  @Column({
    type: 'integer',
    name: 'goalUnit',
  })
  goalUnit: number;

  @Column({
    type: 'numeric',
    name: 'goalAmount',
  })
  goalAmount: string;

  @ManyToOne(() => StoreEntity, (store) => store.products, {
    persistence: false,
    cascade: false,
  })
  @Column({
    type: 'integer',
    name: 'storeId',
  })
  store: StoreEntity;

  @OneToMany(() => SaleEntity, (sale) => sale.product)
  sales: SaleEntity[];

  @CreateDateColumn({
    type: 'timestamp',
    name: 'createdAt',
    default: () => 'NOW()',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    name: 'updatedAt',
    default: () => 'NOW()',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    name: 'deletedAt',
  })
  deletedAt: Date;
}
