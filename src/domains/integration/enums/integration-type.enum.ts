export enum IntegrationTypeEnum {
  pagamento_pix = 'pagamento_pix',
  pagamento_cartao_credito = 'pagamento_cartao_credito',
}

export type IntegrationTypeType = keyof typeof IntegrationTypeEnum;
