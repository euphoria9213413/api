import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { IntegrationRequestEntity } from './integration-request.entity';

@Entity({
  schema: 'public',
  name: 'integration_response',
  synchronize: false,
})
export class IntegrationResponseEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment', {
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'jsonb',
    name: 'body',
  })
  body: string;

  @ManyToOne(
    () => IntegrationRequestEntity,
    (integrationRequest) => integrationRequest.integrationReponses,
    {
      cascade: false,
      persistence: false,
    },
  )
  @Column({
    type: 'integer',
    name: 'integrationRequestId',
  })
  integrationRequest: IntegrationRequestEntity;

  @CreateDateColumn({
    type: 'timestamp',
    name: 'createdAt',
    default: () => 'NOW()',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    name: 'updatedAt',
    default: () => 'NOW()',
  })
  updatedAt: Date;
}
