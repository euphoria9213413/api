import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { IntegrationTypeEnum } from '../enums/integration-type.enum';
import { IntegrationResponseEntity } from './integration-response.entity';

@Entity({
  schema: 'public',
  name: 'integration_request',
  synchronize: false,
})
export class IntegrationRequestEntity extends BaseEntity {
  @PrimaryGeneratedColumn('increment', {
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'jsonb',
    name: 'body',
  })
  body: string;

  @Column({
    type: 'varchar',
    name: 'type',
  })
  type: IntegrationTypeEnum;

  @Column({
    type: 'boolean',
    name: 'situation',
  })
  situation: boolean;

  @OneToMany(
    () => IntegrationResponseEntity,
    (integrationResponse) => integrationResponse.integrationRequest,
    {
      cascade: false,
      persistence: false,
    },
  )
  integrationReponses: IntegrationResponseEntity[];

  @CreateDateColumn({
    type: 'timestamp',
    name: 'createdAt',
    default: () => 'NOW()',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    name: 'updatedAt',
    default: () => 'NOW()',
  })
  updatedAt: Date;
}
