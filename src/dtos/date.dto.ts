export type DateAndHourPeriod = {
  date: string;
  hour: string;
};
