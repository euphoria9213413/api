import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { databaseConfig } from './config/database.config';
import { AccountModule } from './domains/account/account.module';
import { JwtModule } from '@nestjs/jwt';
import env from './config/env';
import { StoreModule } from './domains/store/store.module';
import { ProductModule } from './domains/store/product.module';
import { SaleModule } from './domains/sale/sale.module';
import { GraduateModule } from './domains/store/graduate.module';
import { StorageModule } from './domains/storage/storage.module';
import { MediaModule } from './domains/storage/media.module';
import { MailModule } from './domains/mail/mail.module';
import { PasswordRecoverModule } from './domains/account/password-recover.module';
import { ProfileModule } from './domains/account/profile.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    TypeOrmModule.forRoot(databaseConfig),
    JwtModule.register({
      global: true,
      secret: env.securit.jwt_secret,
      signOptions: { expiresIn: '7d' },
    }),
    ScheduleModule.forRoot(),
    ProfileModule,
    AccountModule,
    StoreModule,
    ProductModule,
    SaleModule,
    GraduateModule,
    StorageModule,
    MediaModule,
    MailModule,
    PasswordRecoverModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
