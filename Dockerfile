###################
# BUILD
###################

FROM node:18.16 As build

WORKDIR /usr/src/app

COPY --chown=node:node package*.json ./
COPY --chown=node:node yarn.lock ./

RUN yarn global add @nestjs/cli@8.0.0

RUN yarn install

COPY --chown=node:node . .

RUN yarn build

USER node

###################
# PRODUCTION
###################

FROM node:18.16 As production

ENV NODE_ENV prod

COPY --chown=node:node --from=build /usr/src/app/node_modules ./node_modules
COPY --chown=node:node --from=build /usr/src/app/dist ./dist

EXPOSE 3000

CMD [ "node", "dist/main.js" ]